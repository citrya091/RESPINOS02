# Respinos Ver. BLE

BLE Profile

| Item                      | UUID                                 |
| ------------------------- | ------------------------------------ |
| Service UUID              | 6E400001-B5A3-F393-E0A9-E50E24DCCA9E |
| Read characteristic UUID  | 6E400002-B5A3-F393-E0A9-E50E24DCCA9E |
| Write characteristic UUID | 6E400003-B5A3-F393-E0A9-E50E24DCCA9E |



## Basic Monitoring

Format message dari app ke device:

```
Untuk mulai:
{"cmd":start,"mode":"basic"}

untuk berhenti
{"cmd":"stop","mode":"basic"}
```

Selama mode basic berlangsung, device mengirimkan data pengukuran setiap 2 detik. 

Contoh data pengukuran mode basic

```
{"payload": {"RR": 123, "SPO2": 123, "HR": 123, "T": 12.34}}
```

Deskripsi:

|  No  | Item | Tipe Data |                         Deskripsi                          |
| :--: | :--: | :-------: | :--------------------------------------------------------: |
|  1   |  RR  |    int    |    Respiratory Rate, frekuensi pernapasan dalam 1 menit    |
|  2   | SPO2 |    int    |            SpO2, saturasi oksigen dalam darah.             |
|  3   |  HR  |    int    | Heart rate, frekuensi detak jantung, dalam beat per minute |
|  3   |  T   |   float   |          Temperatur tubuh, dalam satuan Celcius.           |



## Spirometer

Format message dari app ke device:

```
Untuk mulai:
{"cmd":start,"mode":"spiro"}

untuk berhenti
{"cmd":"stop","mode":"spiro"}
```

Selama pengukuran berlangsung, device mengirim stream data volume setiap 100ms. Data dikirim tanpa format apapun, hanya string dari nilai volume.

|  No  |  Item  | Tipe Data |                          Deskripsi                           |
| :--: | :----: | :-------: | :----------------------------------------------------------: |
|  1   | volume |   float   | volume udara yang keluar/masuk paru-paru, dalam satuan liter |

Hasil spirometry dikirim perangkat apabila setelah perintah untuk berhenti diterima dan manuver spirometry terdeteksi.

Contoh hasil:

```
{
	"value": {
		"FVC": 1.23,
		"FEV1": 1.23,
		"PEF": 1.23,
		"ratio": 0.12,
		"TimeSeriesLength": 123,
		"XYLength": 123
	},
	"graph": {
		"timeseries": [0.00,0.10,...],
		"XY": [
		[0.00,0.22],
		[0.10,0.22],
		...
		]
	}
}
```

Deskripsi hasil:

|  No  |       Item       | Tipe Data | Satuan  |                  Deskripsi                   |
| :--: | :--------------: | :-------: | :------ | :------------------------------------------: |
|  1   |      value       |  object   |         | berisi parameter-parameter hasil pengukuran  |
|  2   |       FVC        |   float   | L       |            Forced Vital Capacity             |
|  3   |       FEV1       |   float   | L       |    First Expiratory Volume in 1st second     |
|  4   |       PEF        |   float   | L/s     |             Peak Expiratory Flow             |
|  5   |      ratio       |    int    |         |                ratio FEV1/FVC                |
|  6   | TimeSeriesLength |    int    |         |       Panjang array grafik timeseries        |
|  7   |     XYLength     |    int    |         |           Panjang array grafik XY            |
|  8   |      graph       |  object   |         |  berisi array untuk kurva hasil pengukuran   |
|  9   |    timeseries    |   array   | L       | kurva volume terhadap waktu selama ekspirasi |
|  10  |        XY        |   array   | [L,L/s] |          kurva flow terhadap volume          |



## Fitur Lainnya

Respinos mempunyai fitur untuk melakukan “pairing”, atau mengunci perangkat dengan akun pengguna. Pairing dilakukan di sisi server, sedangkan un-pairing atau reset pengguna device dilakukan dari perangkat.

Prosedur:

1. pastikan koneksi BLE antara smartphone dengan device tersambung. 
2. pengguna menekan tombol unpair pada device.
3. led mode berkedip, device mengirim perintah untuk reset pengguna ke smartphone
4. smartphone berkomunikasi dengan server untuk reset pengguna.

```
{
    "_id": "respinos01",
    "token": "abcd1234",
    "secret": "X5l4SsKJkXFy253s3X2ISJvqeU8FFi"
}
```

Deskripsi

| No   | item   | deskripsi                                        |
| ---- | ------ | ------------------------------------------------ |
| 1    | ID     | identitas device yang dipakai                    |
| 2    | token  | token untuk mendaftarkan perangkat               |
| 3    | secret | String rahasia yang disepakati server dan device |



## Developer API

API yang digunakan untuk melakukan pengetesan dan debugging. 

List API:

| No   | mode | cmd        | deskripsi                                                    | additional key |
| ---- | ---- | ---------- | ------------------------------------------------------------ | -------------- |
| 1    | dev  | tapflow    | enable/disable print nilai flow ke serial                    |                |
| 2    | dev  | ppgnrm     | enable/disable print nilai normalized ppg ke serial          |                |
| 3    | dev  | ppgreal    | enable/disable print nilai raw ppg ke serial                 |                |
| 4    | dev  | dsdata     | print data yang tersimpan di temperature sensor ke serial dan transmit via ble |                |
| 5    | dev  | setoffset  | ubah nilai offset temperature sensor. Device akan membalas dengan nilai yang tersimpan di sensor. | offset         |
| 6    | dev  | askbattery | meminta kapasitas baterai ke device (dalam persen). Kapasitas baterai ditransmit via ble |                |



Contoh message:

```
tapflow:
{"mode":"dev", "cmd":"tapflow"}

ppgnrm:
{"mode":"dev", "cmd":"ppgnrm"}

ppgreal:
{"mode":"dev", "cmd":"ppgreal"}

dsdata:
{"mode":"dev", "cmd":"dsdata"}
balasan:
Current values on the scratchpad:
Resolution:	12
User data:	1.23

setoffset:
{"mode":"dev", "cmd":"setoffset", "offset":1.23}
balasan:
1.23

askbattery:
{"mode":"dev", "cmd":"askbattery"}
balasan:
{"B":15}
```

askbattery mungkin bisa diimplementasikan di app smartphone, namun mekanisme-nya belum ditentukan. Untuk sementara, device akan mengirim nilai persentasi baterai setelah diminta.

