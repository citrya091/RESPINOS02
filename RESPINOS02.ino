 // RESPINOS05/respinos2
#include "pinAssignment.h"
#include <ArduinoJson.h>
#include <Wire.h>
#include "src/TaskScheduler/TaskScheduler.h"
#include "src/Basic/BasicMeasurement.h"
#include "src/Spiro/SpiroProcessing.h"
#include "src/flow/flowCompensation.h"

#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>
#define SERVICE_UUID           "6E400001-B5A3-F393-E0A9-E50E24DCCA9E" // UART service UUID
#define CHARACTERISTIC_UUID_RX "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
#define CHARACTERISTIC_UUID_TX "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"
// respinos14	fbbe2466
// respinos13	8a7dc63d

// #define DEVICE_NAME "respinos10"
// #define DEVICE_TOKEN "19521f3e"
#define DEVICE_NAME "respinos14"
#define DEVICE_TOKEN "fbbe2466"

#define LOWBAT_TRESHOLD 873
#define SEND_DATA_INTERVAL 2

BLEServer *pServer = NULL;
BLECharacteristic * pTxCharacteristic;
bool oldDeviceConnected = false;

String btmsg;
StaticJsonDocument<256> doc;
StaticJsonDocument<256> graph;
StaticJsonDocument<256> result;
StaticJsonDocument<150> in;
char str_temp[9];
char rxValue;

// State enum
enum RespinosStateEnum {
  RESPINOS_STATE_ERROR = -1,
  RESPINOS_STATE_DISCONNECTED,
  RESPINOS_STATE_READY,
  RESPINOS_STATE_RUNNING,
  RESPINOS_STATE_SPIRO,
  RESPINOS_STATE_RESULT
  
};
RespinosStateEnum systemState = RESPINOS_STATE_DISCONNECTED;
bool RESPINOS_LOW_BATTERY =false;

union floatToBytes {
    char buffer[4];
    float flowReading;

}converter;
float flowCompensated;

/************************ SFM3000***************************/
#include "src/sfm3000/sf05.h"
#include "src/sfm3000/system.h"
etError error;
ft sfm3000_f;
ft flow_atp;
#define OFFSET_FLOW 32000.0F   // offset flow
#define SCALE_FLOW    140.0F   // scale factor flow
const float correction = 1.1618;
/*********************************************************/

Scheduler userScheduler;
void BatteryCheck();
void blinkledpwr();
void getFlow();
Task taskGetFlow( 10, TASK_FOREVER, &getFlow);
Task taskBatteryCheck( 100, TASK_FOREVER, &BatteryCheck);
Task taskBlinkpwr(TASK_SECOND, TASK_FOREVER, &blinkledpwr);
int batteryLvl;

void startMeasurement();
void stopMeasurement();
void startSpirometer();
void stopSpirometer();
void handleTapFlow();
void handleTapFlowAdvanced();
void handleTapNRMSPO2();
void handleTapRSPO2();
// void handleTryPCF();

class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      Serial.println("Client Connected");
	  LED_MODE0_ON;
	  taskGetFlow.enable();
	  systemState=RESPINOS_STATE_READY;
    };

    void onDisconnect(BLEServer* pServer) {
      Serial.println("Client disconnected");
	  if (systemState==RESPINOS_STATE_RUNNING) stopMeasurement();
	  else if(systemState==RESPINOS_STATE_SPIRO) stopSpirometer();
	  LED_MODE0_OFF;
	  taskGetFlow.disable();
	  systemState=RESPINOS_STATE_DISCONNECTED;
    }
};
int16_t batteryPercent;
String btreply;
class MyCallbacks: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
      std::string rxValue = pCharacteristic->getValue();
	  
    
      if (rxValue.length() > 0) {
		btmsg="";
        for (int i = 0; i < rxValue.length(); i++){
          // Serial.print(rxValue[i]);
		  btmsg+=rxValue[i];
		}
        Serial.println(btmsg);
		DeserializationError error = deserializeJson(in, btmsg);
        if (error) {
		  Serial.print(F("deserializeJson() failed: "));
		  Serial.println(error.f_str());
		  return;
		}
		const char *cmd = in["cmd"];
        const char *mode = in["mode"];
	  if(in.containsKey("mode") && in.containsKey("cmd")){
		switch(systemState){
			case RESPINOS_STATE_READY :{
				if( !strcmp(cmd,"start") ){
					if( !strcmp(mode,"spiro") ){
					  startSpirometer();  }
					else if ( !strcmp(mode,"basic") ){
					  startMeasurement();}
				}
			}break;
			case RESPINOS_STATE_RUNNING :{
				if( !strcmp(mode,"basic") ){
					if( !strcmp(cmd,"stop") ){
					  stopMeasurement();  }
				}
			}
			case RESPINOS_STATE_SPIRO :{
				if( !strcmp(mode,"spiro") ){
					if( !strcmp(cmd,"stop") ){
					  stopSpirometer();  }
				}
			}
		}
		if( strcmp(mode,"dev")==0 ){
		  if( strcmp(cmd,"tapflow")==0 ){
			handleTapFlow();
		  }else if( strcmp(cmd,"tapflowadv")==0 ){
			handleTapFlowAdvanced();
		  }else if( strcmp(cmd,"ppgnrm")==0 ){
			handleTapNRMSPO2();
		  }else if( strcmp(cmd,"ppgreal")==0 ){
			handleTapRSPO2();
		  }else if( strcmp(cmd,"dsdata")==0 ){
			  btreply=printSavedDS18();
			  Serial.println(btreply);
		      pTxCharacteristic->setValue(btreply.c_str());
			  pTxCharacteristic->notify();
			  delay(25);
		  }else if( strcmp(cmd,"setoffset")==0 ){
			  float offset = in["offset"];
			  float changed = setToffset(offset);
		      pTxCharacteristic->setValue(String(changed).c_str());
			  pTxCharacteristic->notify();
		  }else if( strcmp(cmd,"askbattery")==0 ){
			  btreply="";
			  doc.clear();
			  doc["B"] = batteryPercent;
			  serializeJson(doc, btreply);
		      pTxCharacteristic->setValue(btreply.c_str());
			  pTxCharacteristic->notify();
			  Serial.print("battery adc: ");
			  Serial.print(batteryLvl);
			  Serial.print(",\t battery %: ");
			  Serial.println(batteryPercent);
		  }
		  // else if( strcmp(cmd,"trypcf")==0 ){
			 // handleTryPCF();
		  // }
		}
	  }
		// if( !strcmp(mode,"spiro") ){
		  // if( !strcmp(cmd,"start") ){
		    // startSpirometer();
		  // } else if( !strcmp(cmd,"stop") ){
		    // stopSpirometer();
		  // }
		// } else if( !strcmp(mode,"basic") ){
		  // if( !strcmp(cmd,"start") ){
		    // startMeasurement();
		  // } else if( !strcmp(cmd,"stop") ){
		    // stopMeasurement();
		  // }
		// }
      }
    }
	
};

void setup(){
  // Serial.begin(115200);
  Serial.begin(230400);
  Wire.begin();
  ioModeSet();
  sensor_setup();
  Serial.println(printSavedDS18());
  
  error=NO_ERROR;
  error = SF05_SoftReset();
  error = SF05_GetFlow(OFFSET_FLOW, SCALE_FLOW, &sfm3000_f);
  addScheduler();
  addDeveloperTask();
  userScheduler.startNow();
  taskBatteryCheck.enable();
  
  // Create the BLE Device
  BLEDevice::init(DEVICE_NAME);

  // Create the BLE Server
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // Create a BLE Characteristic
  pTxCharacteristic = pService->createCharacteristic(
										CHARACTERISTIC_UUID_TX,
										BLECharacteristic::PROPERTY_NOTIFY
									);
                      
  pTxCharacteristic->addDescriptor(new BLE2902());

  BLECharacteristic * pRxCharacteristic = pService->createCharacteristic(
											 CHARACTERISTIC_UUID_RX,
											BLECharacteristic::PROPERTY_WRITE
										);

  pRxCharacteristic->setCallbacks(new MyCallbacks());

  // Start the service
  pService->start();

  // Start advertising
  pServer->getAdvertising()->start();
}

void loop(){
	userScheduler.execute();
	if (systemState==RESPINOS_STATE_READY){
		if(detectLongPush(pairPB0)){
			handleResetUser();
		}
	}
	
	if (systemState==RESPINOS_STATE_RESULT){
	  if(getFIN_FLAG() )
        sendFVCresult(getFVC());
      setFIN_FLAG(false);
      LED_MODE1_OFF;LED_MODE0_ON;
	  systemState=RESPINOS_STATE_READY;
	}
	// disconnecting
    if (systemState==RESPINOS_STATE_DISCONNECTED && oldDeviceConnected) {
        delay(500); // give the bluetooth stack the chance to get things ready
        pServer->startAdvertising(); // restart advertising
        Serial.println("start advertising");
        oldDeviceConnected = systemState==RESPINOS_STATE_READY;
    }
    // connecting
    if (systemState!=RESPINOS_STATE_DISCONNECTED && !oldDeviceConnected) {
		// do stuff here on connecting
        oldDeviceConnected = systemState==RESPINOS_STATE_READY;
    }	 
}

/**************************** Message ******************************/

char *SECRET = "X5l4SsKJkXFy253s3X2ISJvqeU8FFi";
const char *ID_DEVICE = DEVICE_NAME;
const char *TOKEN = DEVICE_TOKEN;
void handleResetUser() {
    doc.clear();
    doc["_id"] = ID_DEVICE;
    doc["token"] = TOKEN;
    doc["secret"] = SECRET;

    String data;
    serializeJson(doc, data);
    pTxCharacteristic->setValue(data.c_str());
    pTxCharacteristic->notify();
    Serial.println("Push to server");
    Serial.println(data.c_str());

}
BasicData basic;
void sendData() {
  basic=GetMonitor();
  btmsg="";
  doc.clear();
  JsonObject payload = doc.createNestedObject("payload");
  payload["RR"] = basic.RR;
  payload["SPO2"] = basic.SPO2;
  payload["HR"] = basic.BPM;
  payload["T"] = basic.T;
  serializeJson(doc, btmsg);
  pTxCharacteristic->setValue(btmsg.c_str());
  pTxCharacteristic->notify();
}

void streamVolume(){
  dtostrf(getVolume(), 6, 3, str_temp);
  dtostrf(getVolume(), 6, 3, str_temp);
  pTxCharacteristic->setValue(str_temp);
  pTxCharacteristic->notify();
}

void sendFVCresult(FVCdata FVC){
	
  btmsg = "{\"value\": ";
  
  result.clear();
  result["FVC"] = FVC.FVC*0.001;
  result["FEV1"] = FVC.FEV1*0.001;
  result["PEF"] = FVC.PEF*0.01;
  result["ratio"] = FVC.rasio*0.01;
  result["TimeSeriesLength"] = FVC.VTLength;
  result["XYLength"] = FVC.VFLength;
  
  serializeJson(result, btmsg);
  pTxCharacteristic->setValue(btmsg.c_str());
  pTxCharacteristic->notify();
  delay(25);
  // Serial.print(btmsg);
  
  btmsg=", \"graph\": {\"timeseries\": [";
  graphpoint XYpoint;

  
  int16_t t=0;
  int16_t end = FVC.VTLength+FVC.VTstart;
  int16_t end2 = (FVC.VTLength+FVC.VTstart)-1;
  for(int i=FVC.VTstart; i<(end); i++){
    dtostrf(getTimeSeries(i), 6, 3, str_temp);
    btmsg+=str_temp;
	
	if(i<end2){
      btmsg+=",";
    }
	t=t+1;
	if(t>=25){
	  pTxCharacteristic->setValue(btmsg.c_str());
      pTxCharacteristic->notify();
      delay(25);
	  // Serial.print(btmsg);
      t=0;
	  btmsg="";
	}
  }
  btmsg+="],";
  pTxCharacteristic->setValue(btmsg.c_str());
  pTxCharacteristic->notify();
  delay(25);
  // Serial.print(btmsg);
  
  btmsg="\"XY\": [ ";
  t=0;
  graph.clear();
  for(int i=FVC.VTstart; i<FVC.VFLength; i++){
  // for(int i=0; i<FVC.VFLength; i++){
    XYpoint=getXY(i);
    copyArray(XYpoint.xy, graph.to<JsonArray>());
    serializeJson(graph, btmsg);
    if(i<FVC.VFLength-1){
      btmsg+=",";
    }
	t=t+1;
	if(t>=15){
	  pTxCharacteristic->setValue(btmsg.c_str());
      pTxCharacteristic->notify();
      delay(25);
	  // Serial.print(btmsg);
      t=0;
	  btmsg="";
	}
  }
  btmsg+="] } }";
  pTxCharacteristic->setValue(btmsg.c_str());
  pTxCharacteristic->notify();
  delay(25);
  // Serial.print(btmsg);
  btmsg="";
}

/**************************** Message ******************************/


/**************************** HW I/O ******************************/
bool detectLongPush(int PB){
  static unsigned long chstart;
  static uint8_t step;
  uint8_t PBstate;
  PBstate=digitalRead(PB);
  switch (step){
    case 0: if(PBstate==LOW) {Serial.println("step=1");step=1; chstart=millis();} break;
    case 1: if(PBstate==HIGH && (millis()-chstart <1200)){Serial.println("step=0");step=0;} else if( millis()-chstart >=1200 ){Serial.println("step=2");step=2;LEDblink(modeLED0); delay(50);LEDblink(modeLED0);}break;
    case 2: if(PBstate==HIGH){step=4;Serial.println("step=4");}else{step=3;Serial.println("step=3");} break;
    case 3: if(PBstate==HIGH){step=4;Serial.println("step=4");} break;
    case 4: {step=0;Serial.println("step=0");} break;
  }
  if(step!=2)
    return false;
  else
    return true;
}

void LEDblink(int LED){

  uint8_t LEDstate;
  LEDstate=digitalRead(LED);
  if (LEDstate) {
    digitalWrite(LED, LOW); //led nyala
  } else {
    digitalWrite(LED, HIGH); //led mati
  }
}

void blinkledpwr(){
  uint8_t LEDstate;
  LEDstate=digitalRead(pwrLED1);
  if (LEDstate) {
    digitalWrite(pwrLED1, LOW);
  } else {
    digitalWrite(pwrLED1, HIGH);
  }
}
void blinkledmod(){
  uint8_t LEDstate;
  LEDstate=digitalRead(modeLED1);
  if (LEDstate) {
    digitalWrite(modeLED1, LOW);
  } else {
    digitalWrite(modeLED1, HIGH);
  }
}
//off: 3.5, lowbat indicator 3.6, indicator off @~3.9
void BatteryCheck(){
  batteryLvl = analogRead(batADC);
  // batteryPercent = round( 0.1255*batteryLvl - 	105.37)*5; //adc 840 - 999
  batteryPercent = round( 0.1481*batteryLvl - 	124.44)*5; //adc 840 - 975
  if(batteryPercent>100)batteryPercent=100;
  if(batteryPercent<0)batteryPercent=0;
  // Serial.println(batteryLvl);
  if(!RESPINOS_LOW_BATTERY){
    if(batteryLvl<LOWBAT_TRESHOLD) {
	  Serial.println("lowbattery");
	  RESPINOS_LOW_BATTERY = true;
	  taskBlinkpwr.enable();
	  LED_PWR0_OFF;
	}
  } else {
    if(batteryLvl>LOWBAT_TRESHOLD+75){
	  Serial.println("almost full");
	  RESPINOS_LOW_BATTERY = false;
	  taskBlinkpwr.disable();
	  LED_PWR0_ON;
	  LED_PWR1_OFF;
	}
  }
}

/**************************** HW I/O ******************************/

void sfm3000(){
  error = SF05_GetFlow(OFFSET_FLOW, SCALE_FLOW, &sfm3000_f);
  flow_atp = sfm3000_f*correction;
}
/**************************** Data Acq ******************************/
float fsbuffer[31];
void getFlow(){
  Wire.requestFrom(8, 4);
  uint8_t index = 0;
  while (Wire.available()){
    converter.buffer[index] = Wire.read();
    index++;
  }
  flowCompensated=turbineComp(converter.flowReading);
  sfm3000();
  shift(flow_atp,fsbuffer,31);
	
}


void processDataS(){
  // getFlow();
  // ProcessFlowS(converter.flowReading);
  
  ProcessFlowS(flowCompensated);
}
void processDataB(){
  // getFlow();
  // ProcessFlowB(converter.flowReading);
  ProcessFlowB(flowCompensated);
}

// processPPG(){
  
// requestTemperature(){
  
  
/**************************** Data Acq ******************************/


/**************************** start / stop ******************************/
void startMeasurement() {
  Serial.println("startMeasurement");
  if (systemState != RESPINOS_STATE_RUNNING) {
    Serial.println("systemState = RUNNING");
    setCommandB(true);
    enableBasicTask();
	LED_MODE1_ON;LED_MODE0_OFF;
    systemState = RESPINOS_STATE_RUNNING;
  }
}

void stopMeasurement() {
  Serial.println("stopMeasurement");
  if (systemState != RESPINOS_STATE_READY) {
    Serial.println("systemState = READY");
    setCommandB(false);
    disableBasicTask();
	LED_MODE1_OFF;LED_MODE0_ON;
    systemState = RESPINOS_STATE_READY;
  }
}

void startSpirometer(){
  Serial.println("startSpirometer");
  if(systemState != RESPINOS_STATE_SPIRO){
    setCommandS(true);
    LED_MODE1_ON;LED_MODE0_OFF;
	// resetbuffer();
    enableSpiroTask();
    systemState=RESPINOS_STATE_SPIRO;
  }
}

void stopSpirometer(){
  Serial.println("stopSpirometer");
  if(systemState == RESPINOS_STATE_SPIRO){
    setCommandS(false);
    disableSpiroTask();
    systemState = RESPINOS_STATE_RESULT;
  }
}
/**************************** start / stop ******************************/

/**************************** scheduler ******************************/
/* Basic */
Task taskProcessFlowB( 10, TASK_FOREVER, &processDataB);
Task taskPPG(7, TASK_FOREVER, &processPPG);
Task taskTemp(10, TASK_FOREVER, requestTemperature);
Task taskSendData(TASK_SECOND * SEND_DATA_INTERVAL, TASK_FOREVER, &sendData);
Task taskBlinkmod(TASK_SECOND, TASK_FOREVER, &blinkledmod);

/* Spiro */
Task taskProcessFlowS( 10, TASK_FOREVER, &processDataS);
Task taskSendVolume(100, TASK_FOREVER, &streamVolume);

/* others */
// Task taskwaitTillZero( 10, TASK_FOREVER, &waitTillZero);

void waitTillZero(){
  // getFlow();
  if(converter.flowReading==0){
	  // taskwaitTillZero.disable();
	  Serial.print("waittillzero disable ");
	  Serial.println(converter.flowReading);
  }
}

void addScheduler(){
  userScheduler.addTask(taskPPG);
  userScheduler.addTask(taskGetFlow);
  userScheduler.addTask(taskProcessFlowB);
  userScheduler.addTask(taskTemp);
  userScheduler.addTask(taskSendData);
  userScheduler.addTask(taskBlinkmod);
 
  userScheduler.addTask(taskProcessFlowS);
  userScheduler.addTask(taskSendVolume);
  
  // userScheduler.addTask(taskwaitTillZero);
  
  userScheduler.addTask(taskBatteryCheck);
  userScheduler.addTask(taskBlinkpwr);
}

void enableBasicTask(){
  // taskwaitTillZero.disable();  
  taskProcessFlowB.enable();
  taskPPG.enable();
  taskTemp.enable();
  taskSendData.enable();
  taskBlinkmod.enable();
	
}
void disableBasicTask(){
  taskProcessFlowB.disable();
  taskPPG.disable();
  taskTemp.disable();
  taskSendData.disable();
  taskBlinkmod.disable();
  // taskwaitTillZero.enable();  
}
void enableSpiroTask(){
  // taskwaitTillZero.disable();  
  taskProcessFlowS.enable();
  taskSendVolume.enable();  	
}
void disableSpiroTask(){
  taskProcessFlowS.disable();
  taskSendVolume.disable();  
  // taskwaitTillZero.enable();  
}

/**************************** scheduler ******************************/

//cmd dev:
	//change Toffset;
	//show userdata DS
	//tap spo2
	//enable print flow
	//enable print volume
	//enable print monitor
void tapFlow();
void tapFlowAdvanced();
void tapRSPO2();
void tapNRMSPO2();
// void trypcfppg();

Task taskTapFlow(10, TASK_FOREVER, &tapFlow);	
Task taskTapFlowAdvanced(10, TASK_FOREVER, &tapFlowAdvanced);	
Task taskTapRSPO2(25, TASK_FOREVER, &tapRSPO2);	
Task taskTapNRMSPO2(25, TASK_FOREVER, &tapNRMSPO2);	
// Task taskTryPCF(25, TASK_FOREVER, &trypcfppg);	

void tapFlow(){
	// if(systemState==RESPINOS_STATE_READY) getFlow();
	Serial.println(flowCompensated);
}
void tapFlowAdvanced(){
	// if(systemState==RESPINOS_STATE_READY) getFlow();
	Serial.print(fsbuffer[30]);
	Serial.print('\t');
	// Serial.print(converter.flowReading);
	Serial.print(getfmult(),4);
	Serial.print('\t');
	Serial.print(flowCompensated);
	Serial.print('\t');
	Serial.print(getsmoothed(),4);
	Serial.print('\t');
	Serial.print(getgain(),4);
	Serial.print('\t');
	Serial.print(getgrf());
	Serial.print('\t');
	Serial.print(getgrp(),4);
	Serial.print('\t');
	Serial.print(getstate(),4);
	// Serial.print(fsbuffer[15]/flowCompensated);
	// Serial.print('\t');
	Serial.println("");
	// Serial.println(converter.flowReading);
}
void tapRSPO2(){
	String ppg;
	ppg=String(tapRealIR());
	ppg+=(taskTapNRMSPO2.isEnabled())? "\t":"\n\r";
	Serial.print(ppg);
}
void tapNRMSPO2(){
	String ppg;
	ppg=String(tapNrmR());
	ppg+="\t";
	ppg+=String(tapNrmIR());
	ppg+="\n\r";
	Serial.print(ppg);
}

// void trypcfppg(){
	// if(PCF(tapRealIR()))
		// taskTryPCF.disable();
// }

void addDeveloperTask(){
	userScheduler.addTask(taskTapFlow);
	userScheduler.addTask(taskTapFlowAdvanced);
	userScheduler.addTask(taskTapRSPO2);
	userScheduler.addTask(taskTapNRMSPO2);
	// userScheduler.addTask(taskTryPCF);
}

void handleTapFlow(){
	if(taskTapFlow.isEnabled())
		taskTapFlow.disable();
	else 
		taskTapFlow.enable();
}
void handleTapFlowAdvanced(){
	if(taskTapFlowAdvanced.isEnabled())
		taskTapFlowAdvanced.disable();
	else 
		taskTapFlowAdvanced.enable();
}
void handleTapNRMSPO2(){
	if(taskTapNRMSPO2.isEnabled()){
		if(taskSendData.isEnabled()||taskTapRSPO2.isEnabled()){ ;
		}else {taskPPG.disable();}
		taskTapNRMSPO2.disable();
	} else {
		if(taskSendData.isEnabled()){ ;
		}else {taskPPG.enable();}
		taskTapNRMSPO2.enable();
	}
}
void handleTapRSPO2(){
	if(taskTapRSPO2.isEnabled()){
		if(taskSendData.isEnabled()||taskTapNRMSPO2.isEnabled()){ ;
		}else {taskPPG.disable();}
		taskTapRSPO2.disable();
	} else {
		if(taskSendData.isEnabled()){ ;
		}else {taskPPG.enable();}
		taskTapRSPO2.enable();
	}
}
// void handleTryPCF(){
	// initpcf();
	// taskTryPCF.enable();
// }
// float FIR[25]={-0.000940, -0.002505, -0.002401, 0.001181,0.004235, -0.003420,-0.027518,-0.054786,-0.052573,0.007644, 0.118646, 0.229415, 0.276000, 0.229415, 0.118646, 0.007644, -0.052573,-0.054786, -0.027518,-0.003420, 0.004235, 0.001181, -0.002401,-0.002505, -0.000940};
// int64_t filter(int64_t raw[25], float C[25]){
	// int64_t Fresult= 0;
  // for(int x=0; x<25; x++){
    // Fresult += raw[x]*C[x];
  // }
  // return Fresult*2.5;
// }