//--------------------------IO-------------------------
#define batADC 34
#define pwrLED0 18
#define pwrLED1 19
#define modeLED0 17
#define modeLED1 5
#define pairPB0 23
#define LED_MODE0_ON digitalWrite(modeLED0,LOW);
#define LED_MODE0_OFF digitalWrite(modeLED0,HIGH);
#define LED_MODE1_ON digitalWrite(modeLED1,LOW);
#define LED_MODE1_OFF digitalWrite(modeLED1,HIGH);
#define LED_PWR0_ON digitalWrite(pwrLED0,LOW);
#define LED_PWR0_OFF digitalWrite(pwrLED0,HIGH);
#define LED_PWR1_ON digitalWrite(pwrLED1,LOW);
#define LED_PWR1_OFF digitalWrite(pwrLED1,HIGH);


//---------------------Temperature---------------------
#define ONE_WIRE_BUS 32
#define  TEMP_SENS_X 35
//--------------------pulse oximetry-------------------
#define AFE_RST 33
#define SPIEN 26
#define AFE_PDN 27
#define ADC_RDY 25

inline bool  ioModeSet(){
  digitalWrite(pairPB0,HIGH);
  pinMode(pairPB0, INPUT);
  pinMode(modeLED0, OUTPUT);
  pinMode(modeLED1, OUTPUT);
  pinMode(pwrLED0, OUTPUT);
  pinMode(pwrLED1, OUTPUT);
  LED_PWR0_ON;
  LED_PWR1_OFF;
  LED_MODE0_OFF;
  LED_MODE1_OFF;
  return true;
}