
float turbineComp(float flowReading);
void smooth(float y, float buffer[], int n, float &gr);
void smooth2(float y, float buffer[],int n,int &zeroCount);
void smooth3(float y, float buffer[],int n,int &zeroCount);
void smooth4(float y, float buffer[],int n,int &zeroCount);
void smoothp(float y, float buffer[],int n,int &zeroCount);
void shift(float y, float buffer[],int n);
void zeroflow_detect();
float getgrp();
float getstate();
float turbineComp2();

float getgain();
float getgrf();
void resetbuffer();
float getsmoothed();
float getfmult();