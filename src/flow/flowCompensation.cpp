
#include <Arduino.h>

#include "flowCompensation.h"
float mult,comp,comp2;
float p, pedit,p2;
float f,fedit;
float grp, grf,peakgrf;
float fbuffer[25],fbuffer2[5];
float pbuffer3[10],pbuffer[10];
float grpbuffer[33];
float compb[5];
int zeroCount;
uint8_t grp_state;

float grp2, pbuffer2[3];
float fc;
float gain;
float ftotal;
	float gbuffer[5];
	float grfbuffer[5];
	float fcbuffer[20];
	float grf_avg,gain_avg;
	float gaingrf;
float turbineComp(float flowReading){
	p=(flowReading==0) ? 0: 250000/flowReading;
	/* if (p<-25000){
		mult = 1.17;
	} else if(p<0){
		// mult = 0.035*abs(p)/4*0.001 + 0.825;
		mult = 0.035*abs(p)/4*0.001 + 0.825;
		// if(mult<0.83) mult=0.83;
	} else if (p==0){
		mult=1;
	}else if(p>0 && p<=1250){
		mult=1.29*abs(p)/4000+0.5;
		if(mult<0.825)mult=0.825;
	}else if(p>0 && p<10500){
		// mult=3.89*(abs(p)/250000)+0.9;
		// mult=3.9*(abs(p)/250000)+0.88;
		mult=2.267*(abs(p)/250000)+0.908;
	}else if(p>=10500 ){
		// mult=1.1248*abs(p)*0.001+0.5468;
	  // mult=1.074;
	  mult=1.075;
	} */
	
	if(p<0){
		// mult = 0.0405*abs(p)/4*0.001 + 0.82;
		mult = 0.037*abs(p)/4*0.001 + 0.8285;
		// mult=(mult<=1.1) ? 1.1:mult;
	}else if(p>0){
		// mult = 0.08*abs(p)/4*0.001 + 0.8;
		mult = 0.09*abs(p)/4*0.001 + 0.8289;
		// mult=1.1248*abs(p)*0.001+0.5468;
	}else mult=1;
	f=mult*flowReading;
	// f=flowReading;
	if(abs(f)<=0.5)f=0;
	// p2=(f==0) ? 0:100/f;
	// smooth2(f,fbuffer,10,zeroCount);
	if(isnan(f) || isinf(f)){
		f=0;
	}
	smooth4(f,fbuffer,25,zeroCount);
	// smoothp(p2,pbuffer3,10,zeroCount);
	// fbuffer[9]=(pbuffer3[9]==0) ? 0:100/pbuffer3[9];
	    if(abs(fbuffer[24])<=1)fbuffer[24]=0;
	    // pedit=(fbuffer[9]==0)?0:250000/fbuffer[9];
	// pedit=pbuffer3[9]*2500;
	// smooth(pedit, pbuffer, 3, grp);
	  
	shift(fbuffer[24],fbuffer2,5);
	fbuffer2[1]=(fbuffer2[0]+fbuffer2[2])/2;
	fedit=(fbuffer2[1]+fbuffer2[2]+fbuffer2[3])/3;
	if(isnan(fedit) || isinf(fedit)){
		fedit=0;
	}
	// fedit=fbuffer2[3];
	// fedit=(fbuffer2[3]+fbuffer2[2]+fbuffer2[1]+fbuffer2[0]+fbuffer[18])/5;
	if(fbuffer2[2]==0 ||fbuffer2[0]==0){
		grf=0;
	// }else 	if(fbuffer2[0]==fbuffer2[2]){
		// ;
	}else grf=(fbuffer2[0]-fbuffer2[2])/2;
	if(isnan(grf) || isinf(grf)){
		grf=0;
	}
	if(abs(fbuffer2[1])<10){
		gaingrf=4;
	} else if(abs(fbuffer2[1])<20){
		gaingrf=1.75;
	} else if(abs(fbuffer2[1])<25){
		gaingrf=1.5;//0.875;
	} else if(abs(fbuffer2[1])<30){
		gaingrf=1.5;//0.75;
	} else if(abs(fbuffer2[1])<50){
		gaingrf=1.25;//0.625;
	}else {
		gaingrf=1;//0.5;
	}
	// if(abs(fbuffer2[1])<10){
		// gaingrf=2.15;
	// } else if(abs(fbuffer2[1])<20){
		// gaingrf=2;
	// } else if(abs(fbuffer2[1])<25){
		// gaingrf=1.75;//0.875;
	// } else if(abs(fbuffer2[1])<30){
		// gaingrf=1.5;//0.75;
	// } else if(abs(fbuffer2[1])<50){
		// gaingrf=1.25;//0.625;
	// }else {
		// gaingrf=1;//0.5;
	// }
	  // if((fbuffer2[1]>0 && grf<0) || (fbuffer2[1]<0 && grf>0))
		// grf=(fbuffer2[0]-fbuffer2[2])*gaingrf;
	// grf=(fbuffer2[2]==0 ||fbuffer2[0]==0) ? 0:(fbuffer2[0]-fbuffer2[2])/2;
	// gain=(fbuffer[24]==0) ? 0:pow((125/abs(fbuffer[24])),2)/50;
	gain=(fbuffer[24]==0) ? 0:pow((100/abs(fbuffer[24])),1.75)*0.033;
	if(isnan(gain) || isinf(gain)){
		gain=0;
	}
	shift(gain,gbuffer,6);
	grf_avg=abs(grfbuffer[0]+grfbuffer[1]+grfbuffer[2]+grfbuffer[3])/4;
	// grf_avg=abs(grfbuffer[1]);
	// grf_avg=abs((grfbuffer[2])+grfbuffer[0]+grfbuffer[1])/3;
	gain_avg=((gbuffer[1]+gbuffer[2])+gbuffer[0])/3;
	// gain_avg=gbuffer[3];
	// shift(grp,grpbuffer,5);
	/* if(grp_state>1){
		// comp=0;
		;
	}else  */if(fedit==0){
		comp=1;
		
	}else if (fedit>0 && grfbuffer[1]>0){
		// grf=grf*gaingrf;
		
	    grf=(abs(fbuffer2[0])>200) ? grf*0.7:grf;
	    // gain_avg=(gain_avg>=0.25) ? 0.25: gain_avg;
	    comp=(abs(fbuffer2[2])<20) ? 1+(grf_avg*gain_avg*0.8):1+(grf_avg*gain_avg*1.58);
		
	}else if (fedit>0 && grfbuffer[1]<0){
		// fedit=fbuffer2[2];
		grf=grf*gaingrf;
		
	    grf=(abs(fbuffer2[1])>200) ? grf*0.45:grf;
		comp=1-(grf_avg*gain_avg*2);
		// comp=1-(grf_avg*gain_avg*1.55*gaingrf);
		
	}else if (fedit<0 && grfbuffer[1]<0){
		// grf=grf*gaingrf;
	    grf=(abs(fbuffer2[0])>200) ? grf*0.7:grf;
	    // gain_avg=(gain_avg>=0.25) ? 0.25: gain_avg;
	    comp=(abs(fbuffer2[2])<20) ? 1+(grf_avg*gain_avg*0.8):1+(grf_avg*gain_avg*1.58);
		
	}else if (fedit<0 && grfbuffer[1]>0){
		// fedit=fbuffer2[2];
		grf=grf*gaingrf;
	    grf=(abs(fbuffer2[1])>200) ? grf*0.45:grf;
		comp=1-(grf_avg*gain_avg*2);
		// comp=1-(grf_avg*gain_avg*1.55*gaingrf);
	}else comp=1;
	shift(grf,grfbuffer,5);
	
	comp=(comp<0) ? 0: comp;
	comp=(comp>4) ? 4: comp;
	if(isnan(comp) || isinf(comp)){
		comp=0;
	}
	
	shift(comp,compb,5);
	zeroflow_detect();
	
	fc=fedit*comp;
	if(isnan(fc) || isinf(fc)){
		fc=0;
	}
	
	if(grp_state>1) {
	  shift(0,fcbuffer,20);
	return 0;
	}else  {
	  shift(fc,fcbuffer,20);
	  return (fcbuffer[0]+fcbuffer[1]+fcbuffer[2]+fcbuffer[3])/4;
	
	}
}
float turbineComp2(){
		if(fbuffer2[1]>0 && grf>0){
		// comp2=(1+0.39*(grf));
		comp2=(1+(grf/abs(peakgrf))*0.41);
	}
	else if(fbuffer2[1]>0 && grf<0){
		// comp2=(1-(grf*((pbuffer[1]/-17500)+0.0045)));
		comp2=1/(1+0.05*abs(grp));
	}
	else if(fbuffer2[1]<0 && grf>0){
		comp2=1/(1+0.05*abs(grp));
		// comp2=(1-(grf*((pbuffer[1]/-27500)+0.0045)));
	}
	else if(fbuffer2[1]<0 && grf<0){
		comp2=(1+(grf/abs(peakgrf))*-0.41);
	} else if(fbuffer2[1]==0){
		comp2=1;
	}
	comp2=(comp2<0)? 0: comp2;
	if(grp_state>1) 
		return 0;
	else 
		return fbuffer2[1]*comp2;
}
float getgrp(){
  // return (grpbuffer[32]+grpbuffer[31])/2;
  return grpbuffer[24];
}
float getstate(){
  return gain_avg;
}

float getgain(){
  return comp;
}
// float getgrf(){
  // return grf;
// }
float getsmoothed(){
  // return fbuffer2[3];
  return fbuffer[24];
}
// float getfmult(){
  // return f;
// }
float gradient;
void smooth(float y, float buffer[], int n, float &gr){
	
	shift(y,buffer,n);
	if(buffer[n-1]==0 || buffer[0]==0){
		gr=0;
	}else if(buffer[0]==buffer[n-1]){
		gr=gr;
	}else gr=(buffer[0]-buffer[n-1])/(n-1);
	// gr=(buffer[n-1]==0 ||buffer[0]==0) ? 0:(buffer[0]-buffer[n-1])/(n-1);
}
void smooth2(float y, float buffer[],int n,int &zeroCount){
	float temp=buffer[9];
	buffer[8]=(buffer[7]-buffer[9])/2+buffer[9];
	shift(y,buffer,n);
		int limit = round(0.8*n);
	float gr;
	float p1,p2;
/* 	if(y==0){
		zeroCount=1;
		gr=0;
	} else if (buffer[1]==buffer[0]){
		zeroCount=zeroCount+1;
	} else {
		// zeroCount=zeroCount+1;
	    if(zeroCount!=1){
		  if( zeroCount <= limit){
	        gr=(buffer[0]-buffer[1])/(zeroCount);
		    for(int i=0; i<zeroCount; i++){
			  buffer[i+1]=buffer[i]-gr;
		    }
		  }
		}
		zeroCount=1;
	} */
	
	if(buffer[1]==buffer[0]){
		if(y==0 && zeroCount>=10) {
		;
		  if(y!=0) 
		    buffer[9]=(buffer[9]==0) ? 0:temp+gradient;
		}else{			
		  zeroCount=zeroCount+1;
		}
	}else{
		// if(buffer[0]==0){
		  // grp=0;
			 
		  // for(int i=0; i<9; i++){
		    // buffer[i+1]=0;
		  // }
		// }else 
		if( (buffer[9]>0 && (buffer[0]<0)) || (buffer[9]<0 && (buffer[0]>0) ) ){
		  p1=(abs(buffer[0])<=0.5)? 0: 100/buffer[0];
		  p2=(abs(buffer[9])<=0.5)? 0: 100/buffer[9];
		  grp=(p1-p2)/10;
		  gr=(buffer[0]-buffer[9])/10; 
		  for(int i=0; i<8; i++){
		    buffer[i+1]=buffer[i]*0.75;
			grpbuffer[i]=grp;
		  }
		  buffer[9]=0;
		} else 
		if(zeroCount!=1){
		  if(buffer[1]==0 && zeroCount>5){
	        gr=(buffer[0]-buffer[1])/5;
		    p1=(abs(buffer[0])<=0.5)? 0: 100/buffer[0];
		    p2=(abs(buffer[1])<=0.5)? 0: 100/buffer[1];
		    grp=(p1-p2)/5;
		    for(int i=0; i<5; i++){
			  buffer[i+1]=buffer[i]-gr;
			  grpbuffer[i]=grp;
		    }
		  }else if( zeroCount <= limit){
	        gr=(buffer[0]-buffer[1])/(zeroCount);
		    p1=(abs(buffer[0])<=0.5)? 0: 100/buffer[0];
		    p2=(abs(buffer[1])<=0.5)? 0: 100/buffer[1];
		    grp=(p1-p2)/zeroCount;
			for(int i=0; i<zeroCount; i++){
			  buffer[i+1]=buffer[i]-gr;
			  grpbuffer[i]=grp;
		    }
		  }else if( zeroCount > limit){
	        gr=(buffer[0]-buffer[1])/(limit); 
		    p1=(abs(buffer[0])<=0.5)? 0: 100/buffer[0];
		    p2=(abs(buffer[1])<=0.5)? 0: 100/buffer[1];
		    grp=(p1-p2)/limit;
			for(int i=0; i<limit; i++){
			  buffer[i+1]=buffer[i]-gr;
			  grpbuffer[i]=grp;
		    }
		  }
		}else{
		  p1=(abs(buffer[0])<=0.5)? 0: 100/buffer[0];
		  p2=(abs(buffer[2])<=0.5)? 0: 100/buffer[2];
		  grp=(p1-p2)/2;
		  grpbuffer[0]=grp;
		}
		zeroCount=1;
		gradient=gr;
	}
	
	shift(grp,grpbuffer,12);
}
void shift(float y, float buffer[],int n){
	for(int i=n-1; i>0; i--){
		buffer[i]=buffer[i-1];
	}
	buffer[0]=y;
}
void zeroflow_detect(){
	if(fedit==0) grp_state=0;
	
	switch(grp_state){
	case 0:{
			if(abs(grf)>peakgrf) peakgrf=abs(grf);
		if(fbuffer2[4]==0 && fedit!=0){
		  grp_state=1;
				comp=1;
				compb[0]=1;
				// fcbuffer[0]=fedit;fcbuffer[1]=fedit;fcbuffer[2]=fedit;
		}
	}break;
	case 1:{
			if(abs(grf)>peakgrf) peakgrf=abs(grf);
			if(compb[3]==0&&compb[0]==0)grp_state=2;
		else 
		if(abs(fedit)<15){
		  if ((fedit>0 && grf<0 && ((grpbuffer[32]+grpbuffer[31])/2)>0.109/* 300 */) ||
		    (fedit<0 && grf>0 && ((grpbuffer[32]+grpbuffer[31])/2)<-0.109/* -300 */)){
			grp_state=2;
		  }
		}else if(abs(fedit)>300){
			if ((fedit>0 && grf<0 && ((grpbuffer[32]+grpbuffer[31])/2)>0.32/* 330 */) ||
		    (fedit<0 && grf>0 && ((grpbuffer[32]+grpbuffer[31])/2)<-0.32/* -330 */)){
			grp_state=2;
		  }	
		}else {
		  if ((fedit>0 && grf<0 && ((grpbuffer[32]+grpbuffer[31])/2)>0.119/* 230 */) ||
		    (fedit<0 && grf>0 && ((grpbuffer[32]+grpbuffer[31])/2)<-0.119/* -230 */)){
			grp_state=2;
		  }	
		}
	}break;
	case 2:{
		if ((fbuffer2[4]<=0 && fedit>0) ||(fbuffer2[4]>=0 && fedit<0)){
				peakgrf=grf;
				// gbuffer[1]=0.5;
				// fbuffer2[1]=fbuffer2[2];
				comp=1;
				compb[0]=1;
				grfbuffer[4]=0;grfbuffer[1]=0;grfbuffer[2]=0;grfbuffer[3]=0;
				// fcbuffer[0]=fedit;fcbuffer[1]=fedit;fcbuffer[2]=fedit;
			grp_state=1;
		}
		if((fbuffer2[3]>0 && grpbuffer[24]<0 &&grfbuffer[0]>0) || (fbuffer2[3]<0 && grpbuffer[24]>0 &&grfbuffer[0]<0)){
				peakgrf=grf;
			grp_state=1;
				comp=1;
				compb[0]=1;
				// fcbuffer[0]=fedit;fcbuffer[1]=fedit;fcbuffer[2]=fedit;
		} 
	}break;
	}
}


int lastzeroCount=1;
int zero=0,idx=0;
int lbuffer[25];
float shiftbuffer[25];
bool process;
int tstate;
void smooth4(float y, float buffer[],int n,int &zeroCount){
	float temp=buffer[24];
	float p1,p2;
	shift(y,buffer,n);
	shift(y,shiftbuffer,n);
	shift(grp,grpbuffer,33);
	process=buffer[0]!=buffer[1];
	if(!process && zeroCount<25){
		zeroCount++;
	}
	if(buffer[0]==0 && zeroCount>=25) {
		tstate=0;
	}
	else {
		switch (tstate){
			case 0:{
				if ( process ){tstate=6;}
				lastzeroCount=2;
				zeroCount=2;
			}break;
			case 1:{
				if (lastzeroCount+zeroCount>=25) {tstate=2;process=true;}
				else if ( process ){
					if(abs(buffer[0])>50 && abs(buffer[1])<10) {process=false;}
					else {tstate=3;}
				}
			}break;
			case 6:{
				lastzeroCount=zeroCount;
				if ( process ){tstate=7;}
			}break;
			case 7:{
				// lastzeroCount=zeroCount;
				if ( process ){
				if(abs(buffer[0])>50 && abs(buffer[1])<10) {tstate=2;}
					else {tstate=8;}
				}
			}break;
			case 8:{
				tstate=4;
			}break;
			case 2:{
				tstate=4;
			}break;
			case 3:{
				tstate=4;
			}break;
			case 4:{
				if ( (buffer[0]!=0 && buffer[1]==0) ||
					 (buffer[0]>0 && buffer[1]<0)   ||
					 (buffer[0]<0 && buffer[1]>0)	)
				{
					tstate=1;
				}
				else if( zeroCount+lastzeroCount>=25){
					// if(lastzeroCount>1){
						// lastzeroCount--;
					// }
					buffer[24]=(buffer[24]==0) ? 0:temp+gradient;
					tstate=5;
				}
			}break;
			case 5:{
				buffer[24]=(buffer[24]==0) ? 0:temp+gradient;
				
				// if(lastzeroCount>1 && zeroCount+lastzeroCount>=25){
						// lastzeroCount--;
					// }
					
				if ( (buffer[0]!=0 && buffer[1]==0) ||
					 (buffer[0]>0 && buffer[1]<0)   ||
					 (buffer[0]<0 && buffer[1]>0)	)
				{
					tstate=1;
				} else if (process && zeroCount+lastzeroCount<25){
					tstate=4;
				}
				if(process) buffer[zeroCount]=buffer[24];
			}break;
		}
		
	}
	if(process){
		if((tstate==3 ||tstate==8 ) && abs(buffer[0])<abs(buffer[1]) ){
			tstate=2;
		}
		for(int i=0; i<zeroCount;i++){
	      buffer[i]=buffer[0];
	      shiftbuffer[i]=shiftbuffer[0];
	    }
		switch (tstate){
			case 0:break;
			case 1:break;
			case 6:break;
			case 7:break;
			case 2:{
				if(25-zeroCount > zeroCount){
					p1=(abs(buffer[0])<=0.5)? 0: 100/buffer[0];
					p2=(abs(buffer[0])<=0.5)? 0: 400/buffer[0];
					grp=(p1-p2)/zeroCount;
					
					gradient=buffer[0]*0.75/zeroCount;
					for (int i=zeroCount; i<2*zeroCount; i++){
						if(i>=25)break;
						buffer[i]=buffer[i-1]-gradient;
						grpbuffer[i]=grp;
						if(abs(buffer[i])<0.5)break;
						zero=i;
					}
					if(24-zero > zeroCount)
						lastzeroCount=zeroCount;
					else lastzeroCount=24-zero;
					
					
					p1=(abs(buffer[zero])<=0.5)? 0: 100/buffer[zero];
					p2=(abs(buffer[zero])<=0.5)? 0: 400/buffer[zero];
					grp=(p1-p2)/lastzeroCount;
					
					gradient=buffer[zero]*0.75/lastzeroCount;
					zero=zero+1;
					for (int i=zero; i<zero+lastzeroCount; i++){
						if(i>=25)break;
						buffer[i]=buffer[i-1]-gradient;
						grpbuffer[i]=grp;
						if(abs(buffer[i])<0.5)break;
					}
				} else {
					lastzeroCount=25-zeroCount;
					
					p1=(abs(buffer[0])<=0.5)? 0: 100/buffer[0];
					p2=(abs(buffer[0])<=0.5)? 0: 1000/buffer[0];
					grp=(p1-p2)/lastzeroCount;
					
					gradient=buffer[0]/lastzeroCount;
					for (int i=zeroCount; i<zeroCount+lastzeroCount; i++){
						if(i>=25)break;
						buffer[i]=buffer[i-1]-gradient;
						grpbuffer[i]=grp;
						if(abs(buffer[i])<0.5)break;
					}
				}
			}break;
			case 3:{
				zero=(buffer[zeroCount]==0) ? 1:(abs(100/buffer[zeroCount])+abs(100/buffer[zeroCount-1]))/2;
				zero=(zero<1) ? 1:zero;
				if(lastzeroCount>(zero)){
					for (int i=zeroCount+1; i<zeroCount+lastzeroCount; i++){
						buffer[i]=0;
					}
					lastzeroCount=zero;
				}
				zero=0;
				
				p1=(abs(buffer[0])<=0.5)? 0: 100/buffer[0];
				p2=(abs(buffer[zeroCount])<=0.5)? 0: 100/buffer[zeroCount];
				grp=(p1-p2)/lastzeroCount;
				
				gradient=(buffer[0]-buffer[zeroCount])/lastzeroCount;
				for(int i=zeroCount; i<zeroCount+lastzeroCount; i++){
					if(i>=25)break;
					buffer[i]=buffer[i-1]-gradient;
					grpbuffer[i]=grp;
					if(abs(buffer[i])<0.5)break;
					zero=i;
				}
					if(abs(buffer[zero])>100){
						buffer[zero+1]=buffer[zero]*0.25;
						break;	
					}
				while(zero<25 && abs(buffer[zero])>0.5){
					gradient=gradient*0.75;
					grp=grp/0.75;
					lastzeroCount=(gradient!=0) ? (abs((buffer[zero]/gradient)-0.5)): 1;
					lastzeroCount=lastzeroCount*0.5;
					lastzeroCount=(lastzeroCount<1) ? 1: lastzeroCount;
					// Serial.println(lastzeroCount);
					if(abs(buffer[zero])-abs(gradient)<0){
					// if(abs(buffer[zero])-gradient<0){
						zero++;
						buffer[zero]=buffer[zero-1]*0.25;
						zero++;
						idx=zero;
						for(int i=idx; i<idx+4; i++){
							if(i>=25)break;
							buffer[i]=buffer[i-1]*0.5;
							grpbuffer[i]=grp;
							if(abs(buffer[i])<0.5)break;
							zero=i;
							
							// Serial.print(zero);
							// Serial.print('\t');
							// Serial.println(buffer[i]);
						}
						zero=25;
						if(abs(buffer[zero])<0.5)break;
					} else {
						zero++;
						idx=zero;
						for(int i=idx; i<idx+lastzeroCount; i++){
							if(i>=25)break;
							if(abs(buffer[i-1])-abs(gradient)<0)break;
							buffer[i]=buffer[i-1]-gradient;
							grpbuffer[i]=grp;
							// if(abs(buffer[i])<0.5)break;
							
							zero=i;
							// Serial.print(zero);
							// Serial.print('\t');
							// Serial.print(gradient);
							// Serial.print('\t');
							// Serial.println(buffer[i]);
						}
							if(abs(buffer[zero])<0.5)break;
					}
				}
			}break;
			case 8:{
				p1=(abs(buffer[0])<=0.5)? 0: 100/buffer[0];
				p2=(abs(buffer[zeroCount])<=0.5)? 0: 100/buffer[zeroCount];
				grp=(p1-p2)/lastzeroCount;
				
				gradient=(buffer[0]-buffer[zeroCount])/lastzeroCount;
				for(int i=zeroCount; i<zeroCount+lastzeroCount; i++){
					if(i>=25)break;
					buffer[i]=buffer[i-1]-gradient;
					grpbuffer[i]=grp;
					if(abs(buffer[i])<0.5)break;
					zero=i;
				}
					if(abs(buffer[zero])>100){
						buffer[zero+1]=buffer[zero]*0.25;
						break;	
					}
				while(zero<25 && abs(buffer[zero])>0.5){
					gradient=gradient*0.85;
					grp=grp/0.85;
					lastzeroCount=(gradient!=0) ? (abs((buffer[zero]/gradient)-0.5)): 1;
					lastzeroCount=lastzeroCount*0.5;
					lastzeroCount=(lastzeroCount<1) ? 1: lastzeroCount;
					// Serial.println(lastzeroCount);
					if(abs(buffer[zero])-abs(gradient)<0){
					// if(abs(buffer[zero])-gradient<0){
						zero++;
						buffer[zero]=buffer[zero-1]*0.25;
						zero++;
						idx=zero;
						for(int i=idx; i<idx+4; i++){
							if(i>=25)break;
							buffer[i]=buffer[i-1]*0.5;
							grpbuffer[i]=grp;
							if(abs(buffer[i])<0.5)break;
							zero=i;
							
							// Serial.print(zero);
							// Serial.print('\t');
							// Serial.println(buffer[i]);
						}
						zero=25;
						if(abs(buffer[zero])<0.5)break;
					} else {
						zero++;
						idx=zero;
						for(int i=idx; i<idx+lastzeroCount; i++){
							if(i>=25)break;
							if(abs(buffer[i-1])-abs(gradient)<0)break;
							buffer[i]=buffer[i-1]-gradient;
							grpbuffer[i]=grp;
							// if(abs(buffer[i])<0.5)break;
							
							zero=i;
							// Serial.print(zero);
							// Serial.print('\t');
							// Serial.print(gradient);
							// Serial.print('\t');
							// Serial.println(buffer[i]);
						}
							if(abs(buffer[zero])<0.5)break;
					}
				}
			}break;
			case 4:{
				p1=(abs(buffer[0])<=0.5)? 0: 100/buffer[0];
				p2=(abs(buffer[zeroCount])<=0.5)? 0: 100/buffer[zeroCount];
				grp=(p1-p2)/lastzeroCount;
				gradient=(buffer[0]-buffer[zeroCount])/lastzeroCount;
				for(int i=zeroCount; i<zeroCount+lastzeroCount; i++){
					buffer[i]=buffer[i-1]-gradient;
					grpbuffer[i]=grp;
				}
			}break;
			case 5:{
				p1=(abs(buffer[0])<=0.5)? 0: 100/buffer[0];
				p2=(abs(buffer[24])<=0.5)? 0: 100/buffer[24];
				grp=(p1==0)? 0 : (p1-p2)/lastzeroCount;
				if(buffer[0]==0){
					gradient=(buffer[zeroCount-1]-buffer[24])/lastzeroCount;
					for(int i=25-lastzeroCount; i<25; i++){
						buffer[i]=0;
						grpbuffer[i]=grp;
					}
				}else{
					gradient=(buffer[0]-buffer[24])/lastzeroCount;
					for(int i=25-lastzeroCount; i<25; i++){
						buffer[i]=buffer[i-1]-gradient;
						grpbuffer[i]=grp;
					}
				}
			}break;
		}
		
		lastzeroCount=zeroCount;
		zeroCount=1;

	}		
}

float getfmult(){
  return shiftbuffer[24];
}

float getgrf(){
  return tstate;
}

void smooth3(float y, float buffer[],int n,int &zeroCount){

	float p1,p2;
	float temp=buffer[19];
	shift(y,buffer,n);
	shift(grp,grpbuffer,25);
	
    if(buffer[1]==buffer[0]){
	  if(zeroCount<19){
		zeroCount=zeroCount+1;
	  }
	  if(zeroCount+lastzeroCount>=20) buffer[19]=(buffer[19]==0) ? 0:temp+gradient;
	}else{
	  if(buffer[1]==0  && buffer[19]==0 ){
		  zero=1;
		  zeroCount=1;
	  }else{
 	    for(int i=0; i<zeroCount;i++){
	      buffer[i]=buffer[0];
	    }
	  }
	  
	  if(buffer[0]==0){
		grp=0;
	    for(int i=0; i<zeroCount-1+lastzeroCount;i++){
	      buffer[i+1]=0;
		  grpbuffer[i]=grp;
	    }
	  }else if(zero==1){
		zero=2;
	  }else if(zero==2){
		if(abs(buffer[zeroCount])<abs((buffer[0]*0.5))){
			// for(int i=zeroCount;i<3*zeroCount;i++){
			  // buffer[i]=buffer[zeroCount];
			// }
			zero=3;
		 }else{
			p1=(abs(buffer[zeroCount-1])<=0.5)? 0: 100/buffer[zeroCount-1];
			p2=(abs(buffer[zeroCount])<=0.5)? 0: 100/buffer[zeroCount];
			grp=(p1-p2)/zeroCount;
			gradient=(buffer[zeroCount-1]-buffer[zeroCount])/zeroCount;
			
			if(2*zeroCount<abs(buffer[zeroCount-1]/gradient)){
				for(int i=zeroCount; i<3*zeroCount; i++){
					buffer[i]=buffer[i-1]-gradient;
					grpbuffer[i]=grp;
					zero=i;
				}
			if(buffer[zero]>0){
				if(gradient>0){
				  for(int i=zero+1; buffer[i-1]-gradient>=0; i++){
				    buffer[i]=buffer[i-1]-gradient;
				    gradient=gradient+gradient*0.5;
					// grpbuffer[i]=grp;
				  }
				}
			}else{
				if(gradient<0){
				  for(int i=zero+1; buffer[i-1]-gradient<=0; i++){
				    buffer[i]=buffer[i-1]-gradient;
				    gradient=gradient+gradient*0.5;
					// grpbuffer[i]=grp;
				  }
				}
			}
			
			}else {
				for(int i=zeroCount; i<zeroCount+abs(buffer[zeroCount-1]/gradient); i++){
					buffer[i]=buffer[i-1]-gradient;
					grpbuffer[i]=grp;
					zero=i;
				}
			if(buffer[zero]>0){
				if(gradient>0){
				  for(int i=zero+1; buffer[i-1]-gradient>=0; i++){
				    buffer[i]=buffer[i-1]-gradient;
				    gradient=gradient+gradient*0.5;
					// grpbuffer[i]=grp;
				  }
				}
			}else{
				if(gradient<0){
				  for(int i=zero+1; buffer[i-1]-gradient<=0; i++){
				    buffer[i]=buffer[i-1]-gradient;
				    gradient=gradient+gradient*0.5;
					// grpbuffer[i]=grp;
				  }
				}
			}
			}
			zero=0;
		} 
	  }else if(zero==3){
		gradient=(buffer[zeroCount-1]-buffer[zeroCount])/lastzeroCount;
		if(2*lastzeroCount<=round(abs(buffer[zeroCount-1]/gradient))){
			for(int i=zeroCount; i<2*lastzeroCount+zeroCount; i++){
				buffer[i]=buffer[i-1]-gradient;
				zero=i;
			}
			if(buffer[zero]>0){
				if(gradient>0){
				  for(int i=zero+1; buffer[i-1]-gradient>=0; i++){
				    buffer[i]=buffer[i-1]-gradient;
				    gradient=gradient+gradient;
					// grpbuffer[i]=grp;
				  }
				}
			}else{
				if(gradient<0){
				  for(int i=zero+1; buffer[i-1]-gradient<=0; i++){
				    buffer[i]=buffer[i-1]-gradient;
				    gradient=gradient+gradient;
					// grpbuffer[i]=grp;
				  }
				}
			}
			/* gradient=(buffer[2*lastzeroCount+zeroCount-1]-buffer[2*lastzeroCount+zeroCount])/lastzeroCount;
			
			for(int i=2*lastzeroCount+zeroCount; i<3*lastzeroCount+zeroCount; i++){
				buffer[i]=buffer[i-1]-gradient;
			} */
		}else {
			for(int i=zeroCount; i<zeroCount+abs(buffer[zeroCount-1]/gradient); i++){
				buffer[i]=buffer[i-1]-gradient;
				zero=i;
			}
			if(buffer[zero]>0){
				if(gradient>0){
				  for(int i=zero+1; buffer[i-1]-gradient>=0; i++){
				    buffer[i]=buffer[i-1]-gradient;
				    gradient=gradient+gradient;
				  	// grpbuffer[i]=grp;
				  }
				}
			}else{
				if(gradient<0){
				  for(int i=zero+1; buffer[i-1]-gradient<=0; i++){
				    buffer[i]=buffer[i-1]-gradient;
				    gradient=gradient+gradient;
					// grpbuffer[i]=grp;
				  }
				}
			}
		}
		zero=0;  
	  }else  if((lastzeroCount+zeroCount)<20 && buffer[zeroCount]!=0 && buffer[0]!=0){
		p1=(abs(buffer[zeroCount-1])<=0.5)? 0: 100/buffer[zeroCount-1];
		p2=(abs(buffer[zeroCount])<=0.5)? 0: 100/buffer[zeroCount];
		grp=(p1-p2)/lastzeroCount;
	    gradient=(buffer[zeroCount-1]-buffer[zeroCount])/lastzeroCount;
		for(int i=zeroCount; i<lastzeroCount+zeroCount; i++){
		  buffer[i]=buffer[i-1]-gradient;
			  grpbuffer[i]=grp;
		}
	  }else if((lastzeroCount+zeroCount)>=20&& buffer[zeroCount]!=0 && buffer[0]!=0){
		buffer[19]=(buffer[19]==0) ? 0:temp+gradient;
		p1=(abs(buffer[zeroCount-1])<=0.5)? 0: 100/buffer[zeroCount-1];
		p2=(abs(buffer[19])<=0.5)? 0: 100/buffer[19];
		grp=(p1-p2)/20;
	    gradient=(buffer[zeroCount-1]-buffer[19])/lastzeroCount;
		for(int i=20-lastzeroCount; i<20; i++){
		  buffer[i]=buffer[i-1]-gradient;
			  grpbuffer[i]=grp;
		}
	  }
      
	  lastzeroCount=zeroCount;
	  zeroCount=1;
	}

	// Serial.print(buffer[0]);
	// Serial.print('\t');
	// Serial.print(buffer[19]);
	// Serial.print('\t');
	// Serial.print(lastzeroCount);
	// Serial.print('\t');
	// Serial.println('\t');	  
}	
	
	

void smoothp(float y, float buffer[],int n,int &zeroCount){
	float temp=buffer[9];
	shift(y,buffer,n);
    if(buffer[1]==buffer[0]){
		if(zeroCount<10){
		  zeroCount=zeroCount+1;
		}else {
			buffer[9]=(buffer[9]==0) ? 0:temp+gradient;
		}
		
	}else{
		if(zeroCount<10){
			if(buffer[0]==0){
			  if(buffer[1]>0) buffer[0]=200;
			  else buffer[0]=-200;
	          gradient=(buffer[0]-buffer[1])/zeroCount;
			  for(int i=0; i<zeroCount; i++){
			    buffer[i+1]=buffer[i]-gradient;
		      }
			  buffer[0]=0;
			}else if (buffer[1]==0){
			  if(buffer[0]>0) buffer[1]=200;
			  else buffer[1]=-200;
	          gradient=(buffer[0]-buffer[1])/zeroCount;
			  for(int i=0; i<zeroCount; i++){
			    buffer[i+1]=buffer[i]-gradient;
		      }
			  buffer[1]=0;
			}else {
			  gradient=(buffer[0]-buffer[1])/zeroCount;
			  for(int i=0; i<zeroCount; i++){
			    buffer[i+1]=buffer[i]-gradient;
		      }
			}
			
		}else {
			buffer[9]=(buffer[9]==0) ? 0:temp+gradient;
			if(buffer[0]==0){
				/* if(buffer[1]>0) buffer[0]=200;
				else buffer[0]=-200;
	            gradient=(buffer[0]-buffer[9])/9;
			    */
				for(int i=0; i<9; i++){
			      buffer[i+1]=0;
		        }
				 buffer[0]=0;
			}else if(buffer[9]==0) {
				if(buffer[0]>0) buffer[9]=200;
				else buffer[9]=-200;
	            gradient=(buffer[0]-buffer[9])/9;
			    for(int i=0; i<9; i++){
			      buffer[i+1]=buffer[i]-gradient;
		        }
				 buffer[9]=0;
			}else {
				gradient=(buffer[0]-buffer[9])/9;
			    for(int i=0; i<9; i++){
			      buffer[i+1]=buffer[i]-gradient;
		        }
			}
		    
		}
		zeroCount=1;
	}
}

void resetbuffer(){
	for(int i=0;i<20;i++){
      fbuffer[i]=0;
      fcbuffer[i]=0;
      grpbuffer[i]=0;
	}
	for(int i=0;i<5;i++){
      compb[i]=0;
      grfbuffer[i]=0;
	}
	for(int i=0;i<4;i++){
      grpbuffer[20+i]=0;
      fbuffer2[i]=0;
      gbuffer[i]=0;
	}
}
