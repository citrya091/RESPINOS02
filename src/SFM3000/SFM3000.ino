
#include <ArduinoJson.h>
// #include "src/typedefs.h"
#include "sf05.h"
#include "system.h"
#define MOTOR_BLOWER 14

//SFM3000

etError error;
ft flow;
//-- Defines -------------------------------------------------------------------
// Offset and scale factors from datasheet (SFM3000).
#define OFFSET_FLOW 32000.0F   // offset flow
#define SCALE_FLOW    140.0F   // scale factor flow

unsigned long last100, last200;
unsigned long now,last,elapsed;

float flow_avg;
float FArray[25];
int readIndex25;
StaticJsonDocument<100> in;
int RR;
void setup(){
  // analogReference(EXTERNAL);
  // pinMode(MOTOR_BLOWER, OUTPUT);
  Serial.begin(230400);
  // MOTOR_SETTING();
  SF05_Init();
  error=NO_ERROR;
  error = SF05_SoftReset();
  error = SF05_GetFlow(OFFSET_FLOW, SCALE_FLOW, &flow);
  delay(1000);
  last100=0;
  last200=0;
}
void loop(){
  
  updateTime();  
  if(now-last100>=100){
    error = SF05_GetFlow(OFFSET_FLOW, SCALE_FLOW, &flow);
	flow_avg=movingAvg3(flow,FArray,flow_avg);
	updateIndex();
	last100=now;
  }
  
  if(now-last200>=200){
	readSerial();
    debugPrint();
    last200=now;
  }

}

void updateTime(){
  last=now;
  now=millis();
  elapsed=now-last;
}

void debugPrint(){
  if(error){
    Serial.println("error");
  }
  else {
    if(flow<-1) RR=1;
    else if(flow>1) RR=0;
    // Serial.print(flow);
	// Serial.print(" ");
    Serial.print(flow_avg/25);
	Serial.println("");
  
  }
}
void readSerial(){
  static char userInput[70];
  static unsigned int i;
  int pwm_set, prop_set;
  while (Serial.available() > 0) {
	userInput[i] =Serial.read();
	i++;
  }
  if(userInput[i-1]=='}'){
    Serial.println(userInput);
	deserializeJson(in, userInput);
    pwm_set  = in["M"];
	prop_set = in["V"];
    for(int x=0; x<70; x++){
      userInput[x]=0;
    }
	i=0;
  }
}
float nvalue;
void updateIndex(){
  readIndex25+=1;
  if(readIndex25>=25){
    readIndex25=0;
  }
}
float movingAvg3(float newValue,float arrayValue[25],float currentTotal){
  float newTotal;
  newTotal=currentTotal - arrayValue[readIndex25] + newValue;
  arrayValue[readIndex25]=newValue;
  return newTotal;
}
// void MOTOR_SETTING(){
	
	// TCCR2A  = (1<<COM2A1)|(1<<COM2A0)|(1<<COM2B1)|(1<<COM2B0)|(1<<WGM21)|(1<<WGM20);  // Set PWM to inverted, 8bit mode
    // TCCR2B  = (1<<CS22)|(1<<CS20);   //prescaler 256
	// set_pwm(&OCR2B, 255);
// }
