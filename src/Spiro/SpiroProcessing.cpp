
#include <Arduino.h>
#include "SpiroProcessing.h"
//-- User Control Flag ------------------------------
bool RUN_FLAG_S;
bool FIN_FLAG;

//-- Flow - breath detection ------------------------------------------------------
float flow;
unsigned long lastSave;
unsigned long now_S, PauseStart_S, inspStart_S, expStart_S;
bool countingPauseEN_S;
uint16_t breathState_S, stateChg_S;
bool inhaleNotValid_S, exhaleNotValid_S;

//-- Flow - Spirometry ------------------------------
int flow_lps;
float volume;
float capacity;
int t, VTstart;
const float BTPS_correction = 0.1667; //10ms

FVCdata FVCsaved, FVCnew;
vf VFsaved;
vf VFnew;

/**************************** Start / Stop ******************************/
void setCommandS(bool flag){
	RUN_FLAG_S=flag;
	if(!RUN_FLAG_S){
	  countingPauseEN_S=false;
	  volume=0;
	  if(breathState_S>1){
		  if(breathState_S==2){ 
		    FVCnew.VTLength=t-FVCnew.VTstart;} 
			delay(500);
	      saveData();
	  } else {
	    FVCreset();
	  }
	  breathState_S=0;
	}
}

void setFIN_FLAG(bool flag){
  FIN_FLAG = flag;
}

bool getFIN_FLAG(){
  return FIN_FLAG;
}

/**************************** Manuver ******************************/
void ProcessFlowS(float readings){
	flow=readings;
	now_S=millis();
	volume+=flow*BTPS_correction;
	if(countingPauseEN_S){
      if(abs(flow)>3 || breathState_S==0) countingPauseEN_S=false;
      if(now_S-PauseStart_S > 10000) {
	  volume=0;stateChg_S=4; countingPauseEN_S=false;}
    }else{
      if(abs(flow)<0.5 && breathState_S != 0){countingPauseEN_S=true;PauseStart_S=now_S;}
    }
	
	switch (breathState_S){
    case 0: {
      if( RUN_FLAG_S ){
	    if(flow>3)
		  stateChg_S=1;
	  } 
    }break;
    case 1: {
      if(now_S-inspStart_S<=150 && flow==0){
	    stateChg_S=4;
	  }
      else if(flow<0 ){
        stateChg_S=2;
      }
      FVCManuverI();
    }break;
    case 2: {
      if(now_S-expStart_S<=150 && abs(flow)<=0){
	    stateChg_S=3;
	  }
      else if(flow>0 ){
        stateChg_S=6;
      }
      FVCManuverE();
    }break;
    case 3: {
      FVCManuverI();
    }break;
  }
  switch(stateChg_S){
    case 0: ;break;
    case 1:{
      inspStart_S=now_S;
      breathState_S=1;
	  FVCinit();
    }break;
	case 2:{ 
	  FVCinit2();
	  expStart_S=now_S;
	  breathState_S=2;
	}break;
	case 3:{ 
	    inspStart_S=now_S;
	    breathState_S=1;
	}break;
	case 4: { 
	  FVCreset();
	  breathState_S=0;
	}break;
	case 6:{
		FVCnew.VTLength=t-FVCnew.VTstart;
		breathState_S=3;
	}break;
  }
  stateChg_S=0;
	
}
bool shift2;
void saveToArray(){
	if(now_S-lastSave >= 20){
      lastSave=now_S;
      if(t<MAX_INDEX){
        VFnew.VFarray[t][0]=capacity;
        VFnew.VFarray[t][1]=flow_lps;
        t++;
		if(stateChg_S==2) shift2=true;
      }
	}
}

void FVCManuverE(){
  flow_lps=round(flow*-1.667);
  capacity-=flow*BTPS_correction;
  if(flow_lps > FVCnew.PEF)FVCnew.PEF=flow_lps;
  if(capacity>FVCnew.FVC)FVCnew.FVC=capacity;
  if(abs( (int)(1000 - (now_S-expStart_S)) )<20) {
    FVCnew.FEV1=capacity;}
  saveToArray();
}

void FVCManuverI(){
  capacity-=flow*BTPS_correction;
  flow_lps=round(flow*-1.667);
  saveToArray();
}

void FVCinit(){
  flow_lps=round(flow*-1.667);
  capacity=flow*-1*BTPS_correction;
  t=1;
}

void FVCinit2(){
  capacity-=flow*BTPS_correction;
  flow_lps=round(flow*-1.667);
  FVCnew.VTstart=(shift2) ? t-2: t-1;
  shift2=false;
  FVCnew.PEF=flow_lps;
  FVCnew.FVC=capacity;
  FVCnew.FEV1=0;
}

void FVCreset(){
    FIN_FLAG=false;
    FVCnew.FEV1=0;
    FVCnew.FVC=0;
    FVCnew.PEF=0;
    FVCnew.rasio=0;
	FVCnew.VTLength=0;
	FVCnew.VFLength=0;
    for(int y; y<MAX_INDEX; y++){
      VFnew.VFarray[y][0]=0;
      VFnew.VFarray[y][1]=0;
    }
}
/**************************** Manuver ******************************/
/**************************** Result ******************************/
void saveData(){
  FIN_FLAG=true;
  FVCnew.VFLength=t;
  // FVCnew.VTLength=t-FVCnew.VTstart;
  FVCnew.FEV1=FVCnew.FEV1-VFnew.VFarray[FVCnew.VTstart][0];
  FVCnew.FVC=FVCnew.FVC-VFnew.VFarray[FVCnew.VTstart][0];
  FVCnew.rasio = round(100*((float)FVCnew.FEV1/FVCnew.FVC));
  FVCsaved=FVCnew;
  // Serial.println(FVCnew.VTstart);
  // Serial.println(VFnew.VFarray[FVCnew.VTstart][0]);
  for(int y=0; y<t; y++){
      VFsaved.VFarray[y][0]=VFnew.VFarray[y][0]-VFnew.VFarray[FVCnew.VTstart][0];
      VFsaved.VFarray[y][1]=VFnew.VFarray[y][1];
  }
}

FVCdata getFVC(){
  return FVCsaved;
}

float getTimeSeries(int idx){
  return VFsaved.VFarray[idx][0]*0.001;
}

graphpoint getXY(int idx){
  graphpoint graph;
  graph.xy[0]=VFsaved.VFarray[idx][0]*0.001;
  graph.xy[1]=VFsaved.VFarray[idx][1]*0.01;
  return graph;
}

float getVolume(){
  return volume*0.001;
}
/**************************** Result ******************************/