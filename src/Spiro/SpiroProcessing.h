
#define MAX_INDEX 1400

typedef struct{
int16_t FVC;
int16_t FEV1;
int16_t PEF;
int16_t rasio;
// int VF[MAX_INDEX][2];
int16_t VTLength;
int16_t VFLength;
int16_t VTstart;
}FVCdata;

typedef struct{

int16_t VFarray[MAX_INDEX][2];

}vf;
typedef struct{

float xy[2];

}graphpoint;

/**************************** Start / Stop ******************************/
void setCommandS(bool flag);
void setFIN_FLAG(bool flag);
bool getFIN_FLAG();

/**************************** Manuver ******************************/
void ProcessFlowS(float readings);
void saveToArray();
void FVCManuverE();
void FVCManuverI();
void FVCinit();
void FVCinit2();
void FVCreset();

/**************************** Result ******************************/
void saveData();
FVCdata getFVC();
float getTimeSeries(int idx);
graphpoint getXY(int idx);
float getVolume();