
#include <Arduino.h>
#include "../../pinAssignment.h"
#include "BasicMeasurement.h"
#include <OneWire.h>
#include <DallasTemperature.h>

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature BDTemp(&oneWire);
DeviceAddress deviceAddress;

int16_t userdata;
float calibration_offset;
bool temp_get=false;

#include "XMV20_SpO2v2.h"
#include "spo2_th.h"
static const int spiClk = 2000000; // 1 MHz
SpO2_AFE44x0  mySpO2(AFE_RST,SPIEN,AFE_PDN,ADC_RDY,SPISettings(spiClk, MSBFIRST, SPI_MODE0));

//-- User Control Flag ------------------------------
bool RUN_FLAG_B;

//-- Flow - breath detection ------------------------
unsigned long now_B, PauseStart_B, inspStart_B, expStart_B;
uint8_t breathState_B;
uint8_t stateChg_B;
bool inhaleNotValid_B, exhaleNotValid_B;
bool countingPauseEN_B=false;

//-- Monitor - Respiratory Rate ---------------------
int TE,TI;
int RRarray[150];
int RRidxAdd, RRidxRem;
int RRnBreath, RRaccTime, RRTotalTime;
int RRtotal, RR;

/**************************** setup  ******************************/

void sensor_setup(){
  BDTemp.begin();
  BDTemp.getAddress(deviceAddress,0);
  BDTemp.setAutoSaveScratchPad(false);
  userdata=BDTemp.getUserData(deviceAddress);
  if(userdata<=10000) calibration_offset=userdata*0.01;
  mySpO2.initIO();
  delay(1000);
  mySpO2.initSPI();
  delay(1000);
}

/**************************** start / stop ******************************/
void setCommandB(bool flag){
    RUN_FLAG_B=flag;
  if(RUN_FLAG_B){
	userdata=BDTemp.getUserData(deviceAddress);
    if(userdata<=10000) calibration_offset=userdata*0.01;
    temp_get=false;
    BDTemp.setWaitForConversion(false);
    BDTemp.requestTemperatures();
  } else {
    breathState_B = 0;
	TI=0;
    TE=0;
    RR=0;
    resetRR();
	countingPauseEN_B=false;
  }
}	

BasicData monitor;
/**************************** Monitoring Data ******************************/

BasicData GetMonitor(){
  if(temp_get){
    temp_get=false;
    BDTemp.setWaitForConversion(false);
    BDTemp.requestTemperatures();
  }
  if(monitor.T<32)monitor.T=0;
  monitor.RR = round(RR*0.1);
  monitor.BPM = round(getBPM());
  monitor.SPO2 =(mySpO2.getRealIR()>=0 && monitor.BPM!=0) ? round(getspO2()):0;
  
  return monitor;
}

/**************************** Temperature ******************************/
//detect di awal 
String printSavedDS18(){
	String scratchp;
	scratchp="Current values on the scratchpad:\n\r";
	scratchp+="Resolution:\t";
	scratchp+=String(BDTemp.getResolution(deviceAddress));
	scratchp+="\n\r User data:\t";
	scratchp+=String(calibration_offset);
	scratchp+="\n\r";
	return scratchp;
}
//task when running
void requestTemperature(){
  if(BDTemp.isConversionComplete() && !temp_get){
    monitor.T=BDTemp.getTempCByIndex(0)+calibration_offset;
    temp_get=true;
  }
}
//get calibration value.
float getToffset(){
  return calibration_offset;
}
//set calibration value
float setToffset(float offset ){
  int16_t saved_offset=round(offset*100);
  //save offset
  BDTemp.getAddress(deviceAddress,0);
  BDTemp.setAutoSaveScratchPad(false);
  BDTemp.setUserData(deviceAddress,saved_offset);
  BDTemp.saveScratchPad(deviceAddress);
  
  //re-read saved calibration
  userdata=BDTemp.getUserData(deviceAddress);
  if(userdata<=10000) calibration_offset=userdata*0.01;
  return calibration_offset;
}
/**************************** Temperature ******************************/

/******************************** spo2 *********************************/

unsigned int lastppg;
void processPPG(){
  mySpO2.refresh();
  
}
int32_t tapRealIR(){return mySpO2.getRealIR();}
int32_t tapNrmIR(){return mySpO2.getNrmIR();}
int32_t tapNrmR(){return mySpO2.getNrmR();}
/******************************** spo2 *********************************/

/******************************** RR *********************************/
void ProcessFlowB(float flow){
  now_B=millis();
  if(countingPauseEN_B){
    if(abs(flow)>0 || breathState_B==0) countingPauseEN_B=false;
    if(now_B-PauseStart_B > 10000) {
		stateChg_B=4; countingPauseEN_B=false;}
    // }
  }else{
    if(abs(flow)<0.5 && breathState_B != 0){countingPauseEN_B=true;PauseStart_B=now_B;}
  }
  switch (breathState_B){  
	case 0: {
	  if( RUN_FLAG_B ){
        if(flow>0)
        stateChg_B=1;
      }
	}break;
	case 1: {
      RRTotalTime=RRaccTime+round((now_B-inspStart_B)*0.01);
      guardRR();
	  if(flow<0 ){
        stateChg_B=2;
      }
	}break;
	case 2: {
      RRTotalTime=RRaccTime+TI+round((now_B-expStart_B)*0.01);
      guardRR();
	  if(flow>0){
        stateChg_B=3;
      }
	}break;
  }
  switch (stateChg_B){  
	case 0:;break;
	case 1: {
      inspStart_B=now_B;
      breathState_B=1;
	}break;
	case 2: {
      TI=round((now_B-inspStart_B)*0.01);
      if(TI<2) inhaleNotValid_B=true;
      else{
        inhaleNotValid_B=false;
      }
      expStart_B=now_B;
      breathState_B=2;
	}break;
	case 3: {
	  breathState_B=1;
      TE=round((now_B-expStart_B)*0.01);
      if(TE<2) exhaleNotValid_B=true;
      else exhaleNotValid_B=false;
      if(!exhaleNotValid_B && !inhaleNotValid_B){
        calcRR();
      }
      inspStart_B=now_B;
	}break;
	case 4: {
      TI=0;
      TE=0;
      RR=0;
      resetRR();
      breathState_B=0;
	}break;
  }
  stateChg_B=0;
}
void calcRR(){
  RRnBreath++;
  RRarray[RRidxAdd] = TI+TE;
  RRaccTime += RRarray[RRidxAdd];
  RRtotal += round((float)6000/(RRarray[RRidxAdd]));
  RR=RRtotal/RRnBreath;
  RRidxAdd++;
  if(RRidxAdd>=150)
  RRidxAdd=0;
}

void guardRR(){
  if(RRTotalTime>200){
    if(RRarray[RRidxRem]!=0){
      RRnBreath--;
      RRaccTime-=RRarray[RRidxRem];
      RRtotal -= round((float)6000/(RRarray[RRidxRem]));
      RRarray[RRidxRem]=0;
      if(RRidxRem!=RRidxAdd)
      RRidxRem++;
      if(RRidxRem>=150)
      RRidxRem=0;
    }
  }
}

void resetRR(){
  RRaccTime=0;
  RRtotal=0;
  RRnBreath=0;
  RRidxAdd=0;
  RRidxRem=0;
  for(int x=0; x<150; x++){
    RRarray[x]=0;
  }
}
/******************************** RR *********************************/
