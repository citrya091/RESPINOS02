
#include <stdint.h>
void shift(int32_t array[],uint8_t len);
void shiftint(int16_t array[],uint8_t len);
void shiftfloat(float array[],uint8_t len);
bool ismin(int32_t in, int32_t array[],uint8_t len);
bool ismax(int32_t in, int32_t array[],uint8_t len);

float mean(float data[], int len);

float stddev (float data[], int len);
int thresholding(float y[],float newData,  int lag, float threshold, float influence);

void resetvar();



void rms(int32_t nrmR,int32_t nrmIR);

void beatacq();

void spoacq();

void smachine();

void customspocalc(int32_t nrmR,int32_t nrmIR);
int getBPM(void);
int getspO2();