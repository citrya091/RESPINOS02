
#include <Arduino.h>
#include "spo2_th.h"
#define beatDepth 5
#define bufDepth     128
int     bufsqrPtr;
bool    bufsqrFilled;
int32_t bufSqrR [bufDepth];
int32_t bufSqrIR[bufDepth];
int32_t acmSqrR;
int32_t acmSqrIR;

int spO2array[5];
float spO2mean[5];
int16_t spO2result;

int32_t Rfiltered[21];
int32_t IRfiltered[21];
int32_t lowpeakR;
int32_t lowpeakIR;
int32_t highpeakR;
int32_t highpeakIR;
float highpeakRarray[10];

int16_t thOut;
bool thFull;
int16_t thidx;

int bpm_array[5];
float bpm_mean[5];
int16_t bpmresult;

uint32_t lastBeatTime=millis();
uint32_t currBeatTime;
int16_t lastReturnedValue = 0;
uint32_t lastReturnTime = 0;
uint32_t beatMiliS[beatDepth];
uint8_t beatPtr;

uint8_t ppgState=0;
bool acquire_en=false;

void shift(int32_t array[],uint8_t len){
  for(int i=len-1; i>0;i--){
    array[i]=array[i-1];
  }
}
void shiftint(int array[],uint8_t len){
  for(int i=len-1; i>0;i--){
    array[i]=array[i-1];
  }
}
void shiftfloat(float array[],uint8_t len){
  for(int i=0; i<len-1; i++){
    array[i]=array[i+1];
  }
}

bool ismin(int32_t in, int32_t array[],uint8_t len){
  int32_t min;
  min=array[len-1];
  for(int i=0; i<len; i++){
    if(array[i]<=min) min=array[i];
  }
  if(in<=min) return true;
  else return false;
}
bool ismax(int32_t in, int32_t array[],uint8_t len){
  int32_t max;
  max=array[len-1];
  for(int i=0; i<len; i++){
    if(array[i]>=max) max=array[i];
  }
  if(in>=max) return true;
  else return false;
}

float mean(float data[], int len){
      float sum=0.0,mean = 0.0;

      int i;
	  sum=0;
	  mean=0;
      for(i=0; i<len; ++i){
       sum +=data[i];
      }
      mean = sum/len;
      return mean;
    }

float stddev (float data[], int len){
      float the_mean = mean(data,len);
      float standardDeviation = 0.0;

      int i;
      for(i=0; i<len; ++i){
        standardDeviation += pow(data[i] - the_mean,2);
      }
      return sqrt(standardDeviation/len);
    }
	float debugavg,debugstd;
int thresholding(float y[],float newData,  int lag, float threshold, float influence){
  float filteredY;
  float avgFilter;
  float stdFilter;
  int detectionOutput;
  bool filterY;

  if(thFull){
    avgFilter=mean(y,lag);
    stdFilter=stddev(y,lag);
    filterY=false;
	debugavg=fabsf(newData - avgFilter);
	debugstd=threshold * stdFilter;
	// Serial.print(stdFilter);
    // if(fabsf(newData - avgFilter) > threshold * stdFilter*1.5){
	  // if(newData > avgFilter) {
        // detectionOutput=2000;
      // } else {
        // detectionOutput = -2000;
      // }
      // filterY = true;
	// } else 
    if(fabsf(newData - avgFilter) > threshold * stdFilter){
	;
      if(newData > avgFilter) {
        detectionOutput=1000;
      } else {
        detectionOutput = -1000;
      }
      filterY = true;
    } else {
      detectionOutput=0;
    }
  } else {
    detectionOutput=0;
  }
  shiftfloat(y,lag);
  y[lag-1]=(filterY)? influence * newData + (1 - influence) * y[lag-2]: newData;

  if(!thFull){
    thidx++;
    if(thidx<lag) thFull=true;
  }

  return detectionOutput;
}

void resetvar(){
  acmSqrR  = 0;
  acmSqrIR = 0;
  for(int i;i<bufDepth;i++){
    bufSqrR [i] = 0 ;
    bufSqrIR[i] = 0;
  }
  bufsqrPtr=0;
  bufsqrFilled=false;
  
  
  for(int i;i<beatDepth;i++){
    beatMiliS [i] = 0 ;
  }
  beatPtr = 0;
  
  for(int i=0; i<5; i++){
    spO2array[i]=0;
    spO2mean[i]=0;
    bpm_mean[i]=0;
    bpm_array[i]=0;
  }
} 
void resetspo(){
 for(int i=0; i<5; i++){
    spO2array[i]=0;
    bpm_array[i]=0;
  }
} 


void rms(int32_t nrmR,int32_t nrmIR){
  if(acquire_en){
    if(bufsqrFilled){
      acmSqrR  -= bufSqrR [bufsqrPtr];
      acmSqrIR -= bufSqrIR[bufsqrPtr];
    }
    int32_t sqrR  = nrmR  * nrmR;
    int32_t sqrIR = nrmIR * nrmIR;
    acmSqrR  += sqrR;
    acmSqrIR += sqrIR;
    bufSqrR [bufsqrPtr] = sqrR ;
    bufSqrIR[bufsqrPtr] = sqrIR;
    
    bufsqrPtr++;
    if(bufsqrPtr>=bufDepth){
      bufsqrPtr=0;
	  bufsqrFilled=true;
    }
  }
}

void beatacq(){
  currBeatTime=millis();
  beatMiliS[beatPtr] = currBeatTime - lastBeatTime;
  lastBeatTime = currBeatTime;
  
  int bpm_acq = 60000/beatMiliS[beatPtr];
  if(bpm_acq<=250 && bpm_acq>=35){
    shiftint(bpm_array,5);
    if(bpm_array[4]!=0){
	  shiftfloat(bpm_mean,5);
	  bpm_mean[4]=bpm_array[2]; 
	}
    bpm_array[0]=bpm_acq;
    if(bpm_mean[0]!=0) bpmresult = round(mean(bpm_mean, 5));
	else bpmresult=0;
  }
  beatPtr++;
  if(beatPtr >= beatDepth){
    beatPtr = 0;
  }
  
}
double ratio ;
int spO2_acq;
void spoacq(){
  ratio = sqrt((double)acmSqrR/acmSqrIR);
  spO2_acq = round(106.497452 - 3.390423 * ratio - 21.365214 * ratio * ratio);
  if(spO2_acq<=100 && spO2_acq>70){
    shiftint(spO2array,5);
    if(spO2array[4]!=0){  
	  shiftfloat(spO2mean,5);
	  spO2mean[4]=spO2array[2];
	}
    spO2array[0]=spO2_acq;
    if(spO2mean[0]!=0) spO2result = round(mean(spO2mean, 5));
	else spO2result=0;
  } //else spO2result=0;
}

void smachine(){
  switch(ppgState){
    case 0: {
	 if(thOut==0 && thFull){
	   ppgState=1;
	   acquire_en=true;
	   lastBeatTime=millis();
	 }
	}break;
	case 1: {
	  if(abs(thOut)==1000){
	    ppgState=2;
		acquire_en=false;
	  }else if(abs(thOut)==2000){
	    ppgState=0;
		resetvar();
	    acquire_en=false;
		resetspo();
	  }
	}break;
    case 2:{
	  if(thOut==0){
	    ppgState=1;
	    acquire_en=true;
		resetspo();
	   lastBeatTime=millis();
	  }else if(abs(thOut)==2000){
	    ppgState=0;
		resetvar();
		resetspo();
	  }
	}break;
  }
}

void customspocalc(int32_t nrmR,int32_t nrmIR){
  static unsigned long lastTime3 = millis();
  if((millis() - lastTime3) > 25){
    lastTime3 += 25;
	shift(Rfiltered,21);
	shift(IRfiltered,21);
    Rfiltered[0]=nrmR;
	IRfiltered[0]=nrmIR;
	if(ismin(IRfiltered[10],IRfiltered,21))lowpeakIR=IRfiltered[10];
	if(ismax(IRfiltered[10],IRfiltered,21))highpeakIR=IRfiltered[10];
	if(ismin(Rfiltered[10],Rfiltered,21)){
	  lowpeakR=Rfiltered[10];
	  
	}
	if(ismax(Rfiltered[10],Rfiltered,21)){ 
	  highpeakR=Rfiltered[10];
	  if((abs(lowpeakR) <40000 &&abs(highpeakR) <40000 )&& (abs(highpeakR) >50 || abs(lowpeakR) >50)){
		if(acquire_en){
          spoacq();	  
		  beatacq();
	    }
	    thOut=thresholding(highpeakRarray,highpeakR,10,5,0.1);
	  } else {
	    thOut=2000;
        spO2result=0;
		bpmresult=0;
		lastReturnedValue=0;
	  }
	  	
	    smachine();
	}
    
	// Serial.print(' ');
	// Serial.print(spO2result);
	// Serial.print(' ');
	// Serial.print(thOut);
	// Serial.print(' ');
	// Serial.print(highpeakR);
	// Serial.print(' ');
	// Serial.print(lowpeakR);
	// Serial.print(' ');
	// Serial.print(nrmR);
	// Serial.print(' ');
	// Serial.print(nrmIR);
	// Serial.println("");
  }
}
int getBPM(void){
    // uint32_t beatMiliSSorted[beatDepth];
    // for(int i=0; i<beatDepth; i++){
        // uint32_t t = beatMiliS[i];
        // int j;
        // for(j=i; (j > 0) && (beatMiliSSorted[j-1] > t); j--){
            // beatMiliSSorted[j] = beatMiliSSorted[j-1];
        // }
        // beatMiliSSorted[j] = t;
    // }
    
	
    // uint32_t currentTime = millis();
    // if((beatMiliSSorted[(beatDepth-3)] - beatMiliSSorted[0]) < (beatMiliSSorted[(beatDepth-3)]/2)){    
        // uint32_t acmBeatMiliS = 0;
        // for(int i=beatDepth-3; i>=0; i--){
            // acmBeatMiliS += beatMiliSSorted[i];
        // }
        
        // int returnedValue = round((60000.0 * (beatDepth-2)) / acmBeatMiliS);
        // lastReturnedValue = returnedValue;
        // lastReturnTime = currentTime;
	
        // return returnedValue;
    // } else {
        // if((currentTime - lastReturnTime) > 5000UL) lastReturnedValue = 1000;
	
        // return lastReturnedValue;
    // }
	return bpmresult;
}
int getspO2(){
  return spO2result;
}