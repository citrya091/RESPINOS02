
#ifndef XMV20_SpO2v2_h
#define XMV20_SpO2v2_h

#include <stdint.h>
#include <SPI.h>

#define CONTROL0    0x00
#define LEDCNTRL    0x22
#define DIAG         0x30

#define buf1Depth    8
#define bufDepth     128
#define nrmMultiplier 65536

// #define beatDepth 12
#define beatDepth 5
#define beatWindow 25
class SpO2_AFE44x0 {
public:
    SpO2_AFE44x0(uint8_t, uint8_t, uint8_t, uint8_t, SPISettings);
    void initIO(void);
    void initSPI(void);
    int  refresh(void);
	
    // inline int32_t getSpO2 (void) {
     // /*  //int32_t spO2 = round(117.5542 - 33.8983 * sqrt((double)acmSqrR/acmSqrIR)); */
	    // double ratio = sqrt((double)acmSqrR/acmSqrIR);
        // int32_t spO2 = round(106.497452 - 3.390423 * ratio - 21.365214 * ratio * ratio);
        // if((spO2 > 102) || (spO2 < -2)) return -1;
        // if(spO2 > 100) return 100;
        // if(spO2 <   0) return   0;
        // return spO2;
    // }
  // /* //inline int32_t getBPM  (void) { return BPM  ; } */
    // int getBPM(void);

    inline int32_t getRealR (void) { return rawR ; }
    inline int32_t getRealIR(void) { return rawIR; }
    inline int32_t getRawR (void) { return buf2R [(buf2Ptr+(bufDepth/2))%bufDepth]/buf1Depth; }
    inline int32_t getRawIR(void) { return buf2IR[(buf2Ptr+(bufDepth/2))%bufDepth]/buf1Depth; }
    inline int32_t getAvgR (void) { return acm2R /((int32_t)buf1Depth*bufDepth); }
    inline int32_t getAvgIR(void) { return acm2IR/((int32_t)buf1Depth*bufDepth); }
    inline int32_t getNrmR (void) { return nrmR ; }
    inline int32_t getNrmIR(void) { return nrmIR; }
    // inline int32_t getSqrR (void) { return acmSqrR  /*/ bufDepth*/; }
    // inline int32_t getSqrIR(void) { return acmSqrIR /*/ bufDepth*/; }
    // inline int32_t getPeakIR(void) { return peakIR; }
    // inline int32_t getLastPeakIR(void) { return lastPeakIR; }

    inline int32_t getRegister(uint8_t address){
        afe44xxWrite(CONTROL0, 0x000001);
        return afe44xxRead(address);
    }

    void afe44xxWrite (uint8_t address, uint32_t data);

private:
    uint8_t RESETpin;
    uint8_t SPISTEpin;
    uint8_t AFE_PDNpin;
    uint8_t ADC_RDYpin;
    SPISettings AFE44x0_SPISettings;
  /* //int32_t BPM; */
    int32_t rawR;
    int32_t rawIR;
    int     buf1Ptr;
    bool    buf1Filled;
    int32_t buf1R [buf1Depth];
    int32_t buf1IR[buf1Depth];
    int32_t acm1R;
    int32_t acm1IR;
    uint8_t fltIdx;
    int64_t fltR [13][25];
    int64_t fltIR[13][25];
    int     buf2Ptr;
    bool    buf2Filled;
    int32_t buf2R [bufDepth];
    int32_t buf2IR[bufDepth];
    int32_t acm2R;
    int32_t acm2IR;
    int32_t nrmR;
    int32_t nrmIR;
    // int32_t bufSqrR [bufDepth];
    // int32_t bufSqrIR[bufDepth];
    // int32_t acmSqrR;
    // int32_t acmSqrIR;
  // /* //uint8_t beatCount; */
    // uint8_t beatPtr;
    // int32_t peakIR, lastPeakIR;
    // uint32_t beatMiliS[beatDepth];
  // /* //uint32_t acmBeatMiliS; */
    int32_t afe44xxRead (uint8_t address);
    void afe44xxInit (void);
};

#define ADC_RDY_PCINT_vect PCINT0_vect
#define ADC_RDY_PCMSK      PCMSK2
#define ADC_RDY_PCIE       PCIE2

#endif
