typedef struct{
int16_t SPO2;
int16_t BPM;
float T;
int16_t RR;
}BasicData;

void sensor_setup();

/**************************** start / stop ******************************/
void setCommandB(bool flag);

/**************************** Monitoring Data ******************************/

BasicData GetMonitor();

/**************************** Temperature ******************************/
//detect di awal 
String printSavedDS18();
//task when running
void requestTemperature(); //task
//get calibration value.
float getToffset();
//set calibration value
float setToffset(float offset );
/**************************** Temperature ******************************/

/******************************** spo2 *********************************/
void processPPG(); //task
int32_t tapRealIR();
int32_t tapNrmIR();
int32_t tapNrmR();
/******************************** spo2 *********************************/

/******************************** RR *********************************/
void ProcessFlowB(float flow); //task
void calcRR();
void guardRR();
void resetRR();