
#include <Arduino.h>

#include "XMV20_SpO2v2.h"
#include "spo2_th.h"
//Sketch uses 216270 bytes (16%) of program storage space. Maximum is 1310720 bytes.
//Global variables use 21536 bytes (6%) of dynamic memory, leaving 306144 bytes for local variables. Maximum is 327680 bytes.

SPIClass * hspi = NULL;
//afe44xx Register definition
#define CONTROL0     0x00
#define LED2STC      0x01
#define LED2ENDC     0x02
#define LED2LEDSTC   0x03
#define LED2LEDENDC  0x04
#define ALED2STC     0x05
#define ALED2ENDC    0x06
#define LED1STC      0x07
#define LED1ENDC     0x08
#define LED1LEDSTC   0x09
#define LED1LEDENDC  0x0a
#define ALED1STC     0x0b
#define ALED1ENDC    0x0c
#define LED2CONVST   0x0d
#define LED2CONVEND  0x0e
#define ALED2CONVST  0x0f
#define ALED2CONVEND 0x10
#define LED1CONVST   0x11
#define LED1CONVEND  0x12
#define ALED1CONVST  0x13
#define ALED1CONVEND 0x14
#define ADCRSTCNT0   0x15
#define ADCRSTENDCT0 0x16
#define ADCRSTCNT1   0x17
#define ADCRSTENDCT1 0x18
#define ADCRSTCNT2   0x19
#define ADCRSTENDCT2 0x1a
#define ADCRSTCNT3   0x1b
#define ADCRSTENDCT3 0x1c
#define PRPCOUNT     0x1d
#define CONTROL1     0x1e
#define SPARE1       0x1f
#define TIAGAIN      0x20
#define TIA_AMB_GAIN 0x21
#define LEDCNTRL     0x22
#define CONTROL2     0x23
#define SPARE2       0x24
#define SPARE3       0x25
#define SPARE4       0x26
#define SPARE4       0x26
#define RESERVED1    0x27
#define RESERVED2    0x28
#define ALARM        0x29
#define LED2VAL      0x2a
#define ALED2VAL     0x2b
#define LED1VAL      0x2c
#define ALED1VAL     0x2d
#define LED2ABSVAL   0x2e
#define LED1ABSVAL   0x2f
#define DIAG         0x30

// Interrupt Variable
volatile int ADC_RDY = 0;

void IRAM_ATTR isr() {
    ADC_RDY++;
}

void SpO2_AFE44x0::afe44xxInit(void)
{
    afe44xxWrite(CONTROL0,0x000000);

    afe44xxWrite(CONTROL0,0x000008);
    afe44xxWrite(CONTROL0,0x000000);

    afe44xxWrite(TIAGAIN,0x000000); // CF = 5pF, RF = 500kR
    afe44xxWrite(TIA_AMB_GAIN,0x000001);

    afe44xxWrite(LEDCNTRL,0x011414);
    afe44xxWrite(CONTROL2,0x000000); // LED_RANGE=100mA, LED=50mA
    afe44xxWrite(CONTROL1,0x010707); // Timers ON, average 3 samples

    afe44xxWrite(PRPCOUNT, 0X001F3F);

    afe44xxWrite(LED2STC, 0X001770);
    afe44xxWrite(LED2ENDC,0X001F3E);
    afe44xxWrite(LED2LEDSTC,0X001770);
    afe44xxWrite(LED2LEDENDC,0X001F3F);
    afe44xxWrite(ALED2STC, 0X000000);
    afe44xxWrite(ALED2ENDC, 0X0007CE);
    afe44xxWrite(LED2CONVST,0X000002);
    afe44xxWrite(LED2CONVEND, 0X0007CF);
    afe44xxWrite(ALED2CONVST, 0X0007D2);
    afe44xxWrite(ALED2CONVEND,0X000F9F);

    afe44xxWrite(LED1STC, 0X0007D0);
    afe44xxWrite(LED1ENDC, 0X000F9E);
    afe44xxWrite(LED1LEDSTC, 0X0007D0);
    afe44xxWrite(LED1LEDENDC, 0X000F9F);
    afe44xxWrite(ALED1STC, 0X000FA0);
    afe44xxWrite(ALED1ENDC, 0X00176E);
    afe44xxWrite(LED1CONVST, 0X000FA2);
    afe44xxWrite(LED1CONVEND, 0X00176F);
    afe44xxWrite(ALED1CONVST, 0X001772);
    afe44xxWrite(ALED1CONVEND, 0X001F3F);

    afe44xxWrite(ADCRSTCNT0, 0X000000);
    afe44xxWrite(ADCRSTENDCT0,0X000000);
    afe44xxWrite(ADCRSTCNT1, 0X0007D0);
    afe44xxWrite(ADCRSTENDCT1, 0X0007D0);
    afe44xxWrite(ADCRSTCNT2, 0X000FA0);
    afe44xxWrite(ADCRSTENDCT2, 0X000FA0);
    afe44xxWrite(ADCRSTCNT3, 0X001770);
    afe44xxWrite(ADCRSTENDCT3, 0X001770);
}

void SpO2_AFE44x0::afe44xxWrite (uint8_t address, uint32_t data)
{
    hspi->beginTransaction(AFE44x0_SPISettings);

    digitalWrite (SPISTEpin, LOW); // enable device
    hspi->transfer (address); // send address to device
    hspi->transfer ((data >> 16) & 0xFF); // write top 8 bits
    hspi->transfer ((data >>  8) & 0xFF); // write middle 8 bits
    hspi->transfer (data & 0xFF); // write bottom 8 bits
    digitalWrite (SPISTEpin, HIGH); // disable device

    hspi->endTransaction();
}

int32_t SpO2_AFE44x0::afe44xxRead (uint8_t address)
{
    int32_t data=0;

    hspi->beginTransaction(AFE44x0_SPISettings);

    digitalWrite (SPISTEpin, LOW); // enable device
    hspi->transfer (address); // send address to device
    data |= ((unsigned long)hspi->transfer(0)<<16); // read top 8 bits data
    data |= ((unsigned long)hspi->transfer(0)<< 8); // read middle 8 bits  data
    data |= hspi->transfer (0); // read bottom 8 bits data
    digitalWrite (SPISTEpin, HIGH); // disable device

    hspi->endTransaction();
    if(data & _BV(23)) data |= ((int32_t)0xFF << 24);
    return data; // return with 24 bits of read data
}

SpO2_AFE44x0::SpO2_AFE44x0(uint8_t RESETpin_i, uint8_t SPISTEpin_i, uint8_t AFE_PDNpin_i, uint8_t ADC_RDYpin_i, SPISettings AFE44x0_SPISettings_i){
    RESETpin   = RESETpin_i  ;
    SPISTEpin  = SPISTEpin_i ;
    AFE_PDNpin = AFE_PDNpin_i;
    ADC_RDYpin = ADC_RDYpin_i;
    AFE44x0_SPISettings = AFE44x0_SPISettings_i;
}

void SpO2_AFE44x0::initIO(void){
    digitalWrite(RESETpin  , LOW );
    digitalWrite(AFE_PDNpin, LOW );
    digitalWrite(SPISTEpin , HIGH);
    pinMode(RESETpin  , OUTPUT);
    pinMode(AFE_PDNpin, OUTPUT);
    pinMode(SPISTEpin , OUTPUT);
    pinMode(ADC_RDYpin , INPUT);

    delay(1);

    digitalWrite(RESETpin  , HIGH);
    digitalWrite(AFE_PDNpin, HIGH);
    digitalWrite(SPISTEpin , HIGH);

    buf1Ptr = 0;
    buf1Filled = false;
    fltIdx = 0;
    acm1R = 0;
    acm1IR = 0;
  /* //beatCount = 0; */
    // beatPtr = 0;
  /* //acmBeatMiliS = 0;
  //BPM = 0; */
    // peakIR = 0;
    // lastPeakIR = 0;
}

void SpO2_AFE44x0::initSPI(void){
    hspi = new SPIClass(HSPI);
    hspi->begin();
    afe44xxInit();

    //PCMSK2 = _BV(1);
    //PCICR = _BV(PCIE2);
    //ADC_RDY_PCMSK = digital_pin_to_bit_mask_PGM[ADC_RDYpin];
    //*digitalPinToPCMSK(ADC_RDYpin) = _BV(digitalPinToPCMSKbit(ADC_RDYpin));
    //PCICR = _BV(digitalPinToPCICRbit(ADC_RDYpin));

    attachInterrupt(ADC_RDYpin, isr, FALLING);
}

int SpO2_AFE44x0::refresh(void){
    if(ADC_RDY){
        ADC_RDY = 0;//--;
        rawR  = getRegister(LED2ABSVAL);
        rawIR = getRegister(LED1ABSVAL);

        if(buf1Filled){
            acm1R  -= buf1R [buf1Ptr];
            acm1IR -= buf1IR[buf1Ptr];
        }
        acm1R  += rawR;
        acm1IR += rawIR;

        buf1R [buf1Ptr] = rawR;
        buf1IR[buf1Ptr] = rawIR;
      //dly1Ptr = (dly1Ptr + 1) % bufDepthDiv2;
      //dly1R [dly1Ptr] = acm1R;
      //dly1IR[dly1Ptr] = acm1IR;

        buf1Ptr++;
        if(buf1Ptr >= buf1Depth){
            buf1Ptr = 0;
            buf1Filled = true;

            fltR[ 0][fltIdx] =   467 *(int64_t)acm1R;
            fltR[ 1][fltIdx] =   462 *(int64_t)acm1R;
            fltR[ 2][fltIdx] =   447 *(int64_t)acm1R;
            fltR[ 3][fltIdx] =   422 *(int64_t)acm1R;
            fltR[ 4][fltIdx] =   387 *(int64_t)acm1R;
            fltR[ 5][fltIdx] =   343 *(int64_t)acm1R;
            fltR[ 6][fltIdx] =   287 *(int64_t)acm1R;
            fltR[ 7][fltIdx] =   222 *(int64_t)acm1R;
            fltR[ 8][fltIdx] =   147 *(int64_t)acm1R;
            fltR[ 9][fltIdx] =    62 *(int64_t)acm1R;
            fltR[10][fltIdx] = (- 33)*(int64_t)acm1R;
            fltR[11][fltIdx] = (-138)*(int64_t)acm1R;
            fltR[12][fltIdx] = (-253)*(int64_t)acm1R;

            int64_t sgsR = ((int64_t) fltR[12][(fltIdx+ 1)%25]
                                    + fltR[11][(fltIdx+ 2)%25]
                                    + fltR[10][(fltIdx+ 3)%25]
                                    + fltR[ 9][(fltIdx+ 4)%25]
                                    + fltR[ 8][(fltIdx+ 5)%25]
                                    + fltR[ 7][(fltIdx+ 6)%25]
                                    + fltR[ 6][(fltIdx+ 7)%25]
                                    + fltR[ 5][(fltIdx+ 8)%25]
                                    + fltR[ 4][(fltIdx+ 9)%25]
                                    + fltR[ 3][(fltIdx+10)%25]
                                    + fltR[ 2][(fltIdx+11)%25]
                                    + fltR[ 1][(fltIdx+12)%25]
                                    + fltR[ 0][(fltIdx+13)%25]
                                    + fltR[ 1][(fltIdx+14)%25]
                                    + fltR[ 2][(fltIdx+15)%25]
                                    + fltR[ 3][(fltIdx+16)%25]
                                    + fltR[ 4][(fltIdx+17)%25]
                                    + fltR[ 5][(fltIdx+18)%25]
                                    + fltR[ 6][(fltIdx+19)%25]
                                    + fltR[ 7][(fltIdx+20)%25]
                                    + fltR[ 8][(fltIdx+21)%25]
                                    + fltR[ 9][(fltIdx+22)%25]
                                    + fltR[10][(fltIdx+23)%25]
                                    + fltR[11][(fltIdx+24)%25]
                                    + fltR[12][ fltIdx       ]) / 5175;

            fltIR[ 0][fltIdx] =   467 *(int64_t)acm1IR;
            fltIR[ 1][fltIdx] =   462 *(int64_t)acm1IR;
            fltIR[ 2][fltIdx] =   447 *(int64_t)acm1IR;
            fltIR[ 3][fltIdx] =   422 *(int64_t)acm1IR;
            fltIR[ 4][fltIdx] =   387 *(int64_t)acm1IR;
            fltIR[ 5][fltIdx] =   343 *(int64_t)acm1IR;
            fltIR[ 6][fltIdx] =   287 *(int64_t)acm1IR;
            fltIR[ 7][fltIdx] =   222 *(int64_t)acm1IR;
            fltIR[ 8][fltIdx] =   147 *(int64_t)acm1IR;
            fltIR[ 9][fltIdx] =    62 *(int64_t)acm1IR;
            fltIR[10][fltIdx] = (- 33)*(int64_t)acm1IR;
            fltIR[11][fltIdx] = (-138)*(int64_t)acm1IR;
            fltIR[12][fltIdx] = (-253)*(int64_t)acm1IR;

            int64_t sgsIR = ((int64_t) fltIR[12][(fltIdx+ 1)%25]
                                     + fltIR[11][(fltIdx+ 2)%25]
                                     + fltIR[10][(fltIdx+ 3)%25]
                                     + fltIR[ 9][(fltIdx+ 4)%25]
                                     + fltIR[ 8][(fltIdx+ 5)%25]
                                     + fltIR[ 7][(fltIdx+ 6)%25]
                                     + fltIR[ 6][(fltIdx+ 7)%25]
                                     + fltIR[ 5][(fltIdx+ 8)%25]
                                     + fltIR[ 4][(fltIdx+ 9)%25]
                                     + fltIR[ 3][(fltIdx+10)%25]
                                     + fltIR[ 2][(fltIdx+11)%25]
                                     + fltIR[ 1][(fltIdx+12)%25]
                                     + fltIR[ 0][(fltIdx+13)%25]
                                     + fltIR[ 1][(fltIdx+14)%25]
                                     + fltIR[ 2][(fltIdx+15)%25]
                                     + fltIR[ 3][(fltIdx+16)%25]
                                     + fltIR[ 4][(fltIdx+17)%25]
                                     + fltIR[ 5][(fltIdx+18)%25]
                                     + fltIR[ 6][(fltIdx+19)%25]
                                     + fltIR[ 7][(fltIdx+20)%25]
                                     + fltIR[ 8][(fltIdx+21)%25]
                                     + fltIR[ 9][(fltIdx+22)%25]
                                     + fltIR[10][(fltIdx+23)%25]
                                     + fltIR[11][(fltIdx+24)%25]
                                     + fltIR[12][ fltIdx       ]) / 5175;

            fltIdx++;
            if(fltIdx >= 25) fltIdx = 0;

            if(buf2Filled){
                acm2R  -= buf2R [buf2Ptr];
                acm2IR -= buf2IR[buf2Ptr];
            }
            acm2R  += sgsR; //acm1R;
            acm2IR += sgsIR; //acm1IR;
            buf2R [buf2Ptr] = sgsR; //acm1R;
            buf2IR[buf2Ptr] = sgsIR; //acm1IR;

            nrmR  = (int64_t)buf2R [(buf2Ptr+14+(bufDepth/2))%bufDepth] * bufDepth * nrmMultiplier / acm2R  - nrmMultiplier;
            nrmIR = (int64_t)buf2IR[(buf2Ptr+14+(bufDepth/2))%bufDepth] * bufDepth * nrmMultiplier / acm2IR - nrmMultiplier;
			rms(nrmR,nrmIR);
            customspocalc(nrmR,nrmIR);
            // if(buf2Filled){
                // acmSqrR  -= bufSqrR [buf2Ptr];
                // acmSqrIR -= bufSqrIR[buf2Ptr];
            // }
            // int32_t sqrR  = nrmR  * nrmR;
            // int32_t sqrIR = nrmIR * nrmIR;
            // acmSqrR  += sqrR;
            // acmSqrIR += sqrIR;
            // bufSqrR [buf2Ptr] = sqrR ;
            // bufSqrIR[buf2Ptr] = sqrIR;

            // /* --------------------------
             // *   Heart Beat Measurement
             // * --------------------------
             // */
            // static bool peakAcquired = false;
          // /* //static int32_t lastPeakIR = 0; */
            // static uint32_t lastBeatTime = millis();
            // static uint32_t currBeatTime = 0;
          // /* //int32_t limitIR = (-1)*sqrt(acmSqrIR/bufDepth); */
            // uint32_t currentTime = millis();
         // /*  //static bool beatFilled = false; */
            // if(nrmIR < peakIR){
                // peakIR = nrmIR;
              // /* //beatCount = 0; */
                // currBeatTime = currentTime;
                // peakAcquired = false;
            // } else if((currentTime - currBeatTime) > (60000UL/240/2)){ //if(beatCount < beatWindow) beatCount++;
                // if(!peakAcquired){
                    // peakAcquired = true;

                    // if(peakIR < (lastPeakIR/2)){
                        // lastPeakIR = peakIR;
                        // beatMiliS[beatPtr] = currBeatTime - lastBeatTime;
                        // lastBeatTime = currBeatTime;

                        // beatPtr++;
                        // if(beatPtr >= beatDepth){
                            // beatPtr = 0;
                        // }
                    // }
                // }
                // peakIR = nrmIR;
            // } else if(peakAcquired)
                // peakIR = nrmIR;

            // if((currentTime - lastBeatTime) > (60000UL*2/30))
                // lastPeakIR = peakIR;

          /*if(beatCount == beatWindow){
                peakIR = 0;

                uint32_t beatTime = currBeatTime - lastBeatTime;

                if(beatTime >= 300) {
                  //if(beatFilled) acmBeatMiliS -= beatMiliS[beatPtr];
                  //acmBeatMiliS += beatTime;
                    beatMiliS[beatPtr] = beatTime;
                    lastBeatTime = currBeatTime;

                  //BPM = round((60000.0 * beatDepth) / acmBeatMiliS);
                  //BPM = round(60000.0 / beatTime);

                    beatPtr++;
                    if(beatPtr >= beatDepth){
                      //beatFilled = true;
                        beatPtr = 0;
                    }
                }
            }*/

            buf2Ptr++;
            if(buf2Ptr >= bufDepth){
                buf2Ptr = 0;
                buf2Filled = true;

              /*if(buf3Filled){
                    acm3R  -= buf3R [buf3Ptr];
                    acm3IR -= buf3IR[buf3Ptr];
                }
                acm3R  += acm2R;
                acm3IR += acm2IR;
                buf3R [buf3Ptr] = acm2R;
                buf3IR[buf3Ptr] = acm2IR;

                buf3Ptr++;
                if(buf3Ptr >= bufDepth){
                    buf3Ptr = 0;
                    buf3Filled = true;
                }*/
            }
        }

        return 1;
    } else
        return 0;
}

// int SpO2_AFE44x0::getBPM(void){
    // static int lastReturnedValue = 0;
    // static uint32_t lastReturnTime = 0;
    // uint32_t beatMiliSSorted[beatDepth];
    // for(int i=0; i<beatDepth; i++){
        // uint32_t t = beatMiliS[i];
        // int j;
        // for(j=i; (j > 0) && (beatMiliSSorted[j-1] > t); j--){
            // beatMiliSSorted[j] = beatMiliSSorted[j-1];
        // }
        // beatMiliSSorted[j] = t;
    // }

    // uint32_t currentTime = millis();

    // if((beatMiliSSorted[(beatDepth-3)] - beatMiliSSorted[0]) < (beatMiliSSorted[(beatDepth-3)]/2)){
        // uint32_t acmBeatMiliS = 0;
        // for(int i=beatDepth-3; i>=0; i--){
            // acmBeatMiliS += beatMiliSSorted[i];
        // }

        // int returnedValue = round((60000.0 * (beatDepth-2)) / acmBeatMiliS);
        // lastReturnedValue = returnedValue;
        // lastReturnTime = currentTime;
        // return returnedValue;
    // } else {
        // if((currentTime - lastReturnTime) > 5000UL) lastReturnedValue = 0;
        // return lastReturnedValue;
    // }
// }
